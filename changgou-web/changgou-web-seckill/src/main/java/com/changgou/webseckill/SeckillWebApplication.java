package com.changgou.webseckill;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author tcwgq
 * @since 2022/6/10 17:02
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableEurekaClient
@EnableFeignClients(basePackages = {
        "com.changgou.search.feign",
        "com.changgou.order.feign",
        "com.changgou.user.feign",
        "com.changgou.goods.feign",
        "com.changgou.pay.feign",
        "com.changgou.seckill.feign"
})
public class SeckillWebApplication {
    public static void main(String[] args) {
        SpringApplication.run(SeckillWebApplication.class, args);
    }

}
