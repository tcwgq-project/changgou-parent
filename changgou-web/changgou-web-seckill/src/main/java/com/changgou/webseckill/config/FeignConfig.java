package com.changgou.webseckill.config;

import com.changgou.common_spring.interceptor.AdminTokenInterceptor;
import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tcwgq
 * @since 2022/6/21 23:26
 */
@Configuration
public class FeignConfig {
    @Bean
    public RequestInterceptor requestInterceptor() {
        return new AdminTokenInterceptor();
    }

}
