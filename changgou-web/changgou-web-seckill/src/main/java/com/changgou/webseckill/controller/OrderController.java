package com.changgou.webseckill.controller;

import com.changgou.common.response.Result;
import com.changgou.seckill.bean.SeckillGoods;
import com.changgou.seckill.feign.SeckillGoodsFeign;
import com.changgou.seckill.feign.SeckillOrderFeign;
import com.changgou.webseckill.anno.AccessLimit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/wseckill/order")
public class OrderController {
    @Autowired
    private SeckillGoodsFeign seckillGoodsFeign;

    @Autowired
    private SeckillOrderFeign orderFeign;

    /**
     * 页面渲染
     */
    @RequestMapping(value = "/ready", method = {RequestMethod.GET, RequestMethod.POST})
    public String ready(Model model) {

        return "list";
    }

    /**
     * 获取当前的时间基准的5个时间段
     */
    @ResponseBody
    @GetMapping("/menus")
    public Result<List<String>> menus() {
        return seckillGoodsFeign.menus();
    }

    @ResponseBody
    @GetMapping(value = "/list")
    public Result<List<SeckillGoods>> list(@RequestParam("time") String time) {
        return seckillGoodsFeign.list(time);
    }

    @ResponseBody
    @PostMapping(value = "/one")
    public Result<SeckillGoods> one(@RequestParam("time") String time, @RequestParam("id") Long id) {
        return seckillGoodsFeign.one(time, id);
    }

    /**
     * 页面渲染
     */
    @RequestMapping(value = "/item", method = {RequestMethod.GET, RequestMethod.POST})
    public String item(Model model) {

        return "item";
    }

    /**
     * 下单
     */
    @ResponseBody
    @PostMapping("/create")
    public Result<Boolean> create(@RequestParam("time") String time, @RequestParam("id") Long id) {
        return orderFeign.create(time, id);
    }

    @ResponseBody
    @GetMapping("/random")
    public Result<String> random() {
        return orderFeign.random();
    }

    @AccessLimit
    @ResponseBody
    @PostMapping("/add")
    public Result<String> add(@RequestParam("time") String time,
                              @RequestParam("id") Long id,
                              @RequestParam("random") String random) {
        return orderFeign.add(time, id, random);
    }

}