package com.changgou.web_search.controller;

import com.changgou.common.response.Result;
import com.changgou.order.bean.Order;
import com.changgou.order.bean.OrderItem;
import com.changgou.order.feign.CartFeign;
import com.changgou.order.feign.OrderFeign;
import com.changgou.user.bean.Address;
import com.changgou.user.feign.AddressFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/worder")
public class OrderController {
    @Autowired
    private CartFeign cartFeign;

    @Autowired
    private OrderFeign orderFeign;

    @Autowired
    private AddressFeign addressFeign;

    /**
     * 页面渲染
     */
    @RequestMapping(value = "/ready", method = {RequestMethod.GET, RequestMethod.POST})
    public String ready(Model model, @RequestParam("skuList") String skuList) {
        Result<List<Address>> listResult = addressFeign.findByUsername();
        List<Address> addresses = listResult.getData();
        Optional<Address> first = addresses.stream().filter(address -> address.getIsDefault().equalsIgnoreCase("1")).findFirst();
        // 默认地址
        first.ifPresent(address -> model.addAttribute("address", address));
        model.addAttribute("addresses", addresses);

        Result<List<OrderItem>> result = cartFeign.list();
        List<OrderItem> items = result.getData();

        List<Long> list = Arrays.stream(skuList.split(",")).map(Long::valueOf).collect(Collectors.toList());
        items = items.stream().filter(orderItem -> list.contains(orderItem.getSkuId())).collect(Collectors.toList());

        // 商品数量与总价格
        Integer totalNum = 0;
        Integer totalPrice = 0;
        for (OrderItem orderItem : items) {
            totalNum += orderItem.getNum();
            totalPrice += orderItem.getMoney();
        }
        model.addAttribute("totalNum", totalNum);
        model.addAttribute("totalPrice", totalPrice);
        model.addAttribute("items", items);

        return "order";
    }

    /**
     * 添加购物车
     */
    @ResponseBody
    @PostMapping("/create")
    public Result<Order> create(@RequestBody Order order) {
        return orderFeign.create(order);
    }

}