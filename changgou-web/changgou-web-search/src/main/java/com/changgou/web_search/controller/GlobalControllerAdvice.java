package com.changgou.web_search.controller;

import com.changgou.common.entity.StatusCode;
import com.changgou.common.exception.ChanggouException;
import com.changgou.common.response.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * TODO 对于json响应，返回json格式结果，对于页面，跳转到通用错误页
 *
 * @author tcwgq
 * @since 2022/5/26 20:27
 */
@Slf4j
@ControllerAdvice
@ResponseBody
public class GlobalControllerAdvice {
    @ExceptionHandler(ChanggouException.class)
    public Result<String> handle(ChanggouException e) {
        log.error("异常出现，message={}", e.getMessage(), e);
        return Result.fail(e.getCode(), e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public Result<String> handle(Exception e) {
        log.error("异常出现，message={}", e.getMessage(), e);
        return Result.fail(StatusCode.ERROR, e.getMessage());
    }

}
