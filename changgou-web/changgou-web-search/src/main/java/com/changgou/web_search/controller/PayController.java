package com.changgou.web_search.controller;

import com.changgou.common.response.Result;
import com.changgou.common_spring.utils.TokenDecoder;
import com.changgou.order.bean.Order;
import com.changgou.order.feign.OrderFeign;
import com.changgou.pay.feign.WeChatPayFeign;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/wpay")
public class PayController {
    @Autowired
    private OrderFeign orderFeign;

    @Autowired
    private WeChatPayFeign weChatPayFeign;

    @Resource
    private RabbitTemplate rabbitTemplate;

    /**
     * 页面渲染
     */
    @RequestMapping(value = "/ready", method = {RequestMethod.GET, RequestMethod.POST})
    public String ready(Model model, @RequestParam("orderId") String orderId) {
        Order order = orderFeign.findById(orderId).getData();
        model.addAttribute("orderId", orderId);
        model.addAttribute("payMoney", order.getPayMoney());

        return "pay";
    }

    @RequestMapping(value = "/confirm", method = {RequestMethod.GET, RequestMethod.POST})
    public String confirm(Model model, @RequestParam("orderId") String orderId) {
        model.addAttribute("orderId", orderId);

        return "payconfirm";
    }

    @RequestMapping(value = "/success", method = {RequestMethod.GET, RequestMethod.POST})
    public String success(Model model,
                          @RequestParam("orderId") String orderId,
                          @RequestParam("payMoney") String payMoney) {
        model.addAttribute("orderId", orderId);
        model.addAttribute("payMoney", payMoney);

        return "paysuccess";
    }

    @RequestMapping(value = "/fail", method = {RequestMethod.GET, RequestMethod.POST})
    public String fail(Model model,
                       @RequestParam("orderId") String orderId,
                       @RequestParam("payMoney") String payMoney) {
        model.addAttribute("orderId", orderId);
        model.addAttribute("payMoney", payMoney);

        return "payfail";
    }

    @RequestMapping(value = "/weixinpay", method = {RequestMethod.GET, RequestMethod.POST})
    public String pay(Model model, @RequestParam("orderId") String orderId) {
        String username = TokenDecoder.getUsername();
        // 根据orderId查询订单
        Result<Order> orderResult = orderFeign.findById(orderId);
        if (orderResult.getData() == null) { // 如果订单不存在
            return "payfail";// 出错页
        }

        Order order = orderResult.getData();
        // 判断支付状态
        if (!"0".equals(order.getPayStatus())) {// 如果不是未支付订单
            return "payfail";// 出错页
        }
        Map<String, String> map = new HashMap<>();
        map.put("username", username);
        map.put("exchange", "order_auto_expire_exchange");
        map.put("queue", "order_auto_expire_queue");// 队列名称
        map.put("routingKey", "");
        // TODO 方便测试，金额固定1分钱
        Result<Map<String, String>> payResult = weChatPayFeign.createNative(orderId, "1", map);
        if (payResult.getData() == null) {
            return "payfail";// 出错页
        }

        Map<String, String> payMap = payResult.getData();
        model.addAllAttributes(payMap);
        model.addAttribute("orderId", order.getId());
        model.addAttribute("payMoney", order.getPayMoney());

        return "weixinpay";
    }

    @ResponseBody
    @RequestMapping(value = "/pay_success", method = {RequestMethod.GET, RequestMethod.POST})
    public Result<String> pay_success(Model model, @RequestParam("orderId") String orderId) {
        rabbitTemplate.convertAndSend("pay_success", "", orderId);
        return Result.success("ok");
    }

    @ResponseBody
    @RequestMapping(value = "/pay_fail", method = {RequestMethod.GET, RequestMethod.POST})
    public Result<String> pay_fail(Model model, @RequestParam("orderId") String orderId) {
        rabbitTemplate.convertAndSend("pay_fail", "", orderId);
        return Result.success("ok");
    }

}