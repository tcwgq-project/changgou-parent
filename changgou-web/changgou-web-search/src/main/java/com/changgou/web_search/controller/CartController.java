package com.changgou.web_search.controller;

import com.changgou.common.response.Result;
import com.changgou.goods.feign.SkuFeign;
import com.changgou.goods.pojo.Sku;
import com.changgou.order.bean.OrderItem;
import com.changgou.order.feign.CartFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/wcart")
public class CartController {
    @Autowired
    private CartFeign cartFeign;

    @Autowired
    private SkuFeign skuFeign;

    /**
     * 添加购物车
     */
    @ResponseBody
    @RequestMapping(value = "/add", method = {RequestMethod.GET, RequestMethod.POST})
    public Result<List<OrderItem>> add(@RequestParam("skuId") Long skuId,
                                       @RequestParam("num") Integer num) {
        cartFeign.add(skuId, num);
        return cartFeign.list();
    }

    /**
     * 添加购物车成功
     */
    @RequestMapping(value = "/success", method = {RequestMethod.GET, RequestMethod.POST})
    public String success(@RequestParam("skuId") Long skuId,
                          @RequestParam("num") Integer num, Model model) {
        Result<Sku> result = skuFeign.findById(skuId);
        model.addAttribute("sku", result.getData());
        model.addAttribute("num", num);

        return "success-cart";
    }

    /**
     * 页面渲染
     */
    @GetMapping("/list")
    public String list(Model model) {
        Result<List<OrderItem>> result = cartFeign.list();
        List<OrderItem> items = result.getData();

        // 商品数量与总价格
        Integer totalNum = 0;
        Integer totalPrice = 0;
        for (OrderItem orderItem : items) {
            totalNum += orderItem.getNum();
            totalPrice += orderItem.getMoney();
        }
        model.addAttribute("totalNum", totalNum);
        model.addAttribute("totalPrice", totalPrice);
        model.addAttribute("items", items);

        return "cart";
    }

}