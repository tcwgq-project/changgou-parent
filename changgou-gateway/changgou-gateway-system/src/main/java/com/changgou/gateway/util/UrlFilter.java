package com.changgou.gateway.util;

/**
 * 描述
 *
 * @version 1.0
 * @since 1.0
 */
public class UrlFilter {
    private static final String ignoreList = "/api/user/login/**,/api/user/add/**";

    /**
     * 用来判断 如果 当前的请求 在 放行的请求中存在,(不需要拦截 :true,否则需要拦截:false)
     */
    public static boolean skip(String uri) {
        String[] split = ignoreList.replace("**", "").split(",");

        for (String path : split) {
            if (path.equals(uri)) {
                // 不需要拦截
                return true;
            }
        }

        // 要拦截
        return false;
    }
}
