package com.changgou.gatewayweb.service;

import org.springframework.http.server.reactive.ServerHttpRequest;

/**
 * @author tcwgq
 * @since 2022/6/21 16:03
 */
public interface AuthService {
    String getJtiFromCookie(ServerHttpRequest request);

    String getTokenFromRedis(String token);

}
