package com.changgou.gatewayweb.filter;

import com.changgou.gatewayweb.service.AuthService;
import com.changgou.gatewayweb.util.UrlFilter;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
public class OAuth2Filter implements GlobalFilter, Ordered {
    public static final String Authorization = "Authorization";
    @Autowired
    private AuthService authService;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        // 获取当前请求对象
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();

        String path = request.getURI().getPath();
        if (UrlFilter.skip(path)) {
            // 放行
            return chain.filter(exchange);
        }

        // 获取客户端token
        String token = getToken(request);
        // 判断redis中token是否存在
        String redisToken = "";
        if (StringUtils.isNotEmpty(token)) {
            if (token.startsWith("bearer ") || token.startsWith("Bearer ")) {
                token = token.substring(7);
            }
            redisToken = authService.getTokenFromRedis(token);
        }
        String loginUrl = "http://localhost:18091/oauth/toLogin";
        if (StringUtils.isEmpty(token) || StringUtils.isEmpty(redisToken)) {
            // 拒绝访问,请求跳转
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            // return response.setComplete();
            return toLoginPage(loginUrl + "?from=" + request.getURI(), exchange);
        }

        // 校验通过 , 请求头增强，放行
        String first = request.getHeaders().getFirst(Authorization);
        if (StringUtils.isEmpty(first)) {
            if (!token.startsWith("bearer ")) {
                token = "bearer " + token;
            }
            // 将token放到请求头中，传递到下个微服务
            request.mutate().header(Authorization, token);
        }

        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }

    private String getToken(ServerHttpRequest request) {
        String first = request.getHeaders().getFirst(Authorization);
        if (StringUtils.isNotEmpty(first)) {
            return first;
        }

        String second = request.getQueryParams().getFirst(Authorization);
        if (StringUtils.isNotEmpty(second)) {
            return second;
        }

        HttpCookie third = request.getCookies().getFirst(Authorization);
        if (third != null) {
            String value = third.getValue();
            if (StringUtils.isNotEmpty(value)) {
                return value;
            }
        }
        return "";
    }

    private Mono<Void> toLoginPage(String loginUrl, ServerWebExchange exchange) {
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(HttpStatus.SEE_OTHER);
        response.getHeaders().set("Location", loginUrl);
        return response.setComplete();
    }

}