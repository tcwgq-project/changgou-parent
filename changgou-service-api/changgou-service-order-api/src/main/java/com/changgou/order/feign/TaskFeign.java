package com.changgou.order.feign;

import com.changgou.common.response.Result;
import com.changgou.order.bean.Task;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * TaskFeign
 *
 * @author tcwgq
 * @since 2022/06/25 10:01
 */
@FeignClient(name = "order-service", contextId = "taskFeign")
@RequestMapping("/task")
public interface TaskFeign {
    /**
     * 新增Task数据
     *
     * @param task
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody Task task);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Long id);

    /**
     * 修改Task数据
     *
     * @param task
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody Task task, @PathVariable Long id);

    /**
     * 根据ID查询Task数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<Task> findById(@PathVariable Long id);

    /**
     * 查询Task全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<Task>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param task
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<Task>> findList(@RequestBody(required = false) Task task);

    /**
     * Task分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Task>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * Task分页条件搜索实现
     *
     * @param task
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Task>> findPage(@RequestBody(required = false) Task task, @PathVariable int page, @PathVariable int size);

}