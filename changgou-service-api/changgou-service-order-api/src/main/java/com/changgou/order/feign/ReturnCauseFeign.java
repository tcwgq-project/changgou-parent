package com.changgou.order.feign;

import com.changgou.common.response.Result;
import com.changgou.order.bean.ReturnCause;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * ReturnCauseFeign
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
@FeignClient(name = "order-service", contextId = "returnCauseFeign")
@RequestMapping("/returnCause")
public interface ReturnCauseFeign {
    /**
     * 新增ReturnCause数据
     *
     * @param returnCause
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody ReturnCause returnCause);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Integer id);

    /**
     * 修改ReturnCause数据
     *
     * @param returnCause
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody ReturnCause returnCause, @PathVariable Integer id);

    /**
     * 根据ID查询ReturnCause数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<ReturnCause> findById(@PathVariable Integer id);

    /**
     * 查询ReturnCause全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<ReturnCause>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param returnCause
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<ReturnCause>> findList(@RequestBody(required = false) ReturnCause returnCause);

    /**
     * ReturnCause分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<ReturnCause>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * ReturnCause分页条件搜索实现
     *
     * @param returnCause
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<ReturnCause>> findPage(@RequestBody(required = false) ReturnCause returnCause, @PathVariable int page, @PathVariable int size);

}