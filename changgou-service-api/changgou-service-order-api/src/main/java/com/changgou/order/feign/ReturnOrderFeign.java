package com.changgou.order.feign;

import com.changgou.common.response.Result;
import com.changgou.order.bean.ReturnOrder;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * ReturnOrderFeign
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
@FeignClient(name = "order-service", contextId = "returnOrderFeign")
@RequestMapping("/returnOrder")
public interface ReturnOrderFeign {
    /**
     * 新增ReturnOrder数据
     *
     * @param returnOrder
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody ReturnOrder returnOrder);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Long id);

    /**
     * 修改ReturnOrder数据
     *
     * @param returnOrder
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody ReturnOrder returnOrder, @PathVariable Long id);

    /**
     * 根据ID查询ReturnOrder数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<ReturnOrder> findById(@PathVariable Long id);

    /**
     * 查询ReturnOrder全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<ReturnOrder>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param returnOrder
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<ReturnOrder>> findList(@RequestBody(required = false) ReturnOrder returnOrder);

    /**
     * ReturnOrder分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<ReturnOrder>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * ReturnOrder分页条件搜索实现
     *
     * @param returnOrder
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<ReturnOrder>> findPage(@RequestBody(required = false) ReturnOrder returnOrder, @PathVariable int page, @PathVariable int size);

}