package com.changgou.order.feign;

import com.changgou.common.response.Result;
import com.changgou.order.bean.OrderItem;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "order-service", contextId = "cartFeign")
@RequestMapping("/cart")
public interface CartFeign {
    /**
     * 添加购物车
     *
     * @param skuId
     * @param num
     * @return
     */
    @PostMapping("/add")
    Result<String> add(@RequestParam("skuId") Long skuId, @RequestParam("num") Integer num);

    /***
     * 查询用户购物车列表
     * @return
     */
    @GetMapping("/list")
    Result<List<OrderItem>> list();

}