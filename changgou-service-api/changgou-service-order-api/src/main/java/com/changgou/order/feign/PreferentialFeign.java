package com.changgou.order.feign;

import com.changgou.common.response.Result;
import com.changgou.order.bean.Preferential;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * PreferentialFeign
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
@FeignClient(name = "order-service", contextId = "preferentialFeign")
@RequestMapping("/preferential")
public interface PreferentialFeign {
    /**
     * 新增Preferential数据
     *
     * @param preferential
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody Preferential preferential);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Integer id);

    /**
     * 修改Preferential数据
     *
     * @param preferential
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody Preferential preferential, @PathVariable Integer id);

    /**
     * 根据ID查询Preferential数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<Preferential> findById(@PathVariable Integer id);

    /**
     * 查询Preferential全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<Preferential>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param preferential
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<Preferential>> findList(@RequestBody(required = false) Preferential preferential);

    /**
     * Preferential分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Preferential>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * Preferential分页条件搜索实现
     *
     * @param preferential
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Preferential>> findPage(@RequestBody(required = false) Preferential preferential, @PathVariable int page, @PathVariable int size);

}