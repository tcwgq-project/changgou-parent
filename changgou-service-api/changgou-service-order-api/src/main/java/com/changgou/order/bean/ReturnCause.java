package com.changgou.order.bean;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * ReturnCauseBean
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
@Data
@Table(name = "tb_return_cause")
public class ReturnCause implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;// ID

    @Column(name = "cause")
    private String cause;// 原因

    @Column(name = "seq")
    private Integer seq;// 排序

    @Column(name = "status")
    private String status;// 是否启用

}
