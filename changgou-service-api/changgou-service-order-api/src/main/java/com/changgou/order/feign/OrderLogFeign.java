package com.changgou.order.feign;

import com.changgou.common.response.Result;
import com.changgou.order.bean.OrderLog;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * OrderLogFeign
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
@FeignClient(name = "order-service", contextId = "orderLogFeign")
@RequestMapping("/orderLog")
public interface OrderLogFeign {
    /**
     * 新增OrderLog数据
     *
     * @param orderLog
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody OrderLog orderLog);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable String id);

    /**
     * 修改OrderLog数据
     *
     * @param orderLog
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody OrderLog orderLog, @PathVariable String id);

    /**
     * 根据ID查询OrderLog数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<OrderLog> findById(@PathVariable String id);

    /**
     * 查询OrderLog全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<OrderLog>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param orderLog
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<OrderLog>> findList(@RequestBody(required = false) OrderLog orderLog);

    /**
     * OrderLog分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<OrderLog>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * OrderLog分页条件搜索实现
     *
     * @param orderLog
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<OrderLog>> findPage(@RequestBody(required = false) OrderLog orderLog, @PathVariable int page, @PathVariable int size);

}