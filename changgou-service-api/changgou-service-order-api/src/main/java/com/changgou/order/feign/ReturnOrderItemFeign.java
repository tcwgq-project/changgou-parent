package com.changgou.order.feign;

import com.changgou.common.response.Result;
import com.changgou.order.bean.ReturnOrderItem;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * ReturnOrderItemFeign
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
@FeignClient(name = "order-service", contextId = "returnOrderItemFeign")
@RequestMapping("/returnOrderItem")
public interface ReturnOrderItemFeign {
    /**
     * 新增ReturnOrderItem数据
     *
     * @param returnOrderItem
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody ReturnOrderItem returnOrderItem);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Long id);

    /**
     * 修改ReturnOrderItem数据
     *
     * @param returnOrderItem
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody ReturnOrderItem returnOrderItem, @PathVariable Long id);

    /**
     * 根据ID查询ReturnOrderItem数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<ReturnOrderItem> findById(@PathVariable Long id);

    /**
     * 查询ReturnOrderItem全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<ReturnOrderItem>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param returnOrderItem
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<ReturnOrderItem>> findList(@RequestBody(required = false) ReturnOrderItem returnOrderItem);

    /**
     * ReturnOrderItem分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<ReturnOrderItem>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * ReturnOrderItem分页条件搜索实现
     *
     * @param returnOrderItem
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<ReturnOrderItem>> findPage(@RequestBody(required = false) ReturnOrderItem returnOrderItem, @PathVariable int page, @PathVariable int size);

}