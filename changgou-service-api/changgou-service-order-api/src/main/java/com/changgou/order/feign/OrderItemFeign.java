package com.changgou.order.feign;

import com.changgou.common.response.Result;
import com.changgou.order.bean.OrderItem;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * OrderItemFeign
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
@FeignClient(name = "order-service", contextId = "orderItemFeign")
@RequestMapping("/orderItem")
public interface OrderItemFeign {
    /**
     * 新增OrderItem数据
     *
     * @param orderItem
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody OrderItem orderItem);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable String id);

    /**
     * 修改OrderItem数据
     *
     * @param orderItem
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody OrderItem orderItem, @PathVariable String id);

    /**
     * 根据ID查询OrderItem数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<OrderItem> findById(@PathVariable String id);

    /**
     * 查询OrderItem全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<OrderItem>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param orderItem
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<OrderItem>> findList(@RequestBody(required = false) OrderItem orderItem);

    /**
     * OrderItem分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<OrderItem>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * OrderItem分页条件搜索实现
     *
     * @param orderItem
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<OrderItem>> findPage(@RequestBody(required = false) OrderItem orderItem, @PathVariable int page, @PathVariable int size);

}