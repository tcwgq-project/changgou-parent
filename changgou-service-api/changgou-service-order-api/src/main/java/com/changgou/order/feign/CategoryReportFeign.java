package com.changgou.order.feign;

import com.changgou.common.response.Result;
import com.changgou.order.bean.CategoryReport;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * CategoryReportFeign
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
@FeignClient(name = "order-service", contextId = "categoryReportFeign")
@RequestMapping("/categoryReport")
public interface CategoryReportFeign {
    /**
     * 新增CategoryReport数据
     *
     * @param categoryReport
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody CategoryReport categoryReport);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Date id);

    /**
     * 修改CategoryReport数据
     *
     * @param categoryReport
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody CategoryReport categoryReport, @PathVariable Date id);

    /**
     * 根据ID查询CategoryReport数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<CategoryReport> findById(@PathVariable Date id);

    /**
     * 查询CategoryReport全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<CategoryReport>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param categoryReport
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<CategoryReport>> findList(@RequestBody(required = false) CategoryReport categoryReport);

    /**
     * CategoryReport分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<CategoryReport>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * CategoryReport分页条件搜索实现
     *
     * @param categoryReport
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<CategoryReport>> findPage(@RequestBody(required = false) CategoryReport categoryReport, @PathVariable int page, @PathVariable int size);

}