package com.changgou.pay.feign;

import com.changgou.common.response.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * WeChatPayFeign
 *
 * @author tcwgq
 * @since 2022/6/16 18:23
 */
@FeignClient(name = "pay-service", contextId = "weChatPayFeign")
@RequestMapping("/wechat/pay")
public interface WeChatPayFeign {
    /**
     * 创建二维码连接地址返回给前端 生成二维码图片
     *
     * @param out_trade_no 外部订单号
     * @param total_fee    金额
     */
    @PostMapping("/create/native")
    Result<Map<String, String>> createNative(@RequestParam("out_trade_no") String out_trade_no,
                                             @RequestParam("total_fee") String total_fee,
                                             @RequestBody Map<String, String> attach);

    /**
     * 根据交易订单号 来查询订单的状态
     *
     * @param out_trade_no 外部订单号
     */
    @PostMapping("/status/query")
    Result<Map<String, String>> queryStatus(@RequestParam("out_trade_no") String out_trade_no);

    /**
     * 关单
     *
     * @param out_trade_no 外部订单号
     * @return
     */
    @PostMapping("/close")
    Result<Map<String, String>> cls(@RequestParam("out_trade_no") String out_trade_no);

    /**
     * 接收 微信支付通知的结果  结果(以流的形式传递过来)
     */
    @PostMapping("/notify/url")
    String notify(HttpServletRequest request);

}