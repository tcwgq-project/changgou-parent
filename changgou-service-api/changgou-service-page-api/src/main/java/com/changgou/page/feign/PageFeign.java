package com.changgou.page.feign;

import com.changgou.common.response.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 描述
 *
 * @version 1.0
 * @since 1.0
 */
@FeignClient(name = "page-service", contextId = "pageFeign")
@RequestMapping("/page")
public interface PageFeign {

    /***
     * 根据SpuID生成静态页
     * @param id spuID
     */
    @GetMapping("/createHtml/{id}")
    Result<String> createHtml(@PathVariable(name = "id") Long id);

}