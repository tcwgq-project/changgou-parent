package com.changgou.goods.feign;

import com.changgou.common.response.Result;
import com.changgou.goods.pojo.Sku;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * SkuFeign
 *
 * @author tcwgq
 * @since 2022/05/29 20:23
 */
@FeignClient(name = "goods-service", contextId = "skuFeign")
@RequestMapping("/sku")
public interface SkuFeign {
    /**
     * 新增Sku数据
     *
     * @param sku
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody Sku sku);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Long id);

    /**
     * 修改Sku数据
     *
     * @param sku
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody Sku sku, @PathVariable Long id);

    /**
     * 减库存
     *
     * @param map Map<SkuId, num>
     * @return
     */
    @PostMapping(value = "/decrease")
    Result<String> decrease(@RequestBody Map<Long, Integer> map);

    /**
     * 加库存
     *
     * @param map Map<SkuId, num>
     * @return
     */
    @PostMapping(value = "/increase")
    Result<String> increase(@RequestBody Map<Long, Integer> map);

    /**
     * 根据ID查询Sku数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<Sku> findById(@PathVariable Long id);

    /**
     * 查询Sku全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<Sku>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param sku
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<Sku>> findList(@RequestBody(required = false) Sku sku);

    /**
     * Sku分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Sku>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * Sku分页条件搜索实现
     *
     * @param sku
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Sku>> findPage(@RequestBody(required = false) Sku sku, @PathVariable int page, @PathVariable int size);

}