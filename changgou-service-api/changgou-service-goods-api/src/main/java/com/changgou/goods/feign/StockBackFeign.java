package com.changgou.goods.feign;

import com.changgou.common.response.Result;
import com.changgou.goods.pojo.StockBack;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * StockBackFeign
 *
 * @author tcwgq
 * @since 2022/05/29 20:23
 */
@FeignClient(name = "goods-service", contextId = "stockBackFeign")
@RequestMapping("/stockBack")
public interface StockBackFeign {
    /**
     * 新增StockBack数据
     *
     * @param stockBack
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody StockBack stockBack);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable String id);

    /**
     * 修改StockBack数据
     *
     * @param stockBack
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody StockBack stockBack, @PathVariable String id);

    /**
     * 根据ID查询StockBack数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<StockBack> findById(@PathVariable String id);

    /**
     * 查询StockBack全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<StockBack>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param stockBack
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<StockBack>> findList(@RequestBody(required = false) StockBack stockBack);

    /**
     * StockBack分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<StockBack>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * StockBack分页条件搜索实现
     *
     * @param stockBack
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<StockBack>> findPage(@RequestBody(required = false) StockBack stockBack, @PathVariable int page, @PathVariable int size);

}