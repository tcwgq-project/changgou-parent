package com.changgou.goods.feign;

import com.changgou.common.response.Result;
import com.changgou.goods.pojo.Spu;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * SpuFeign
 *
 * @author tcwgq
 * @since 2022/05/29 20:23
 */
@FeignClient(name = "goods-service", contextId = "spuFeign")
@RequestMapping("/spu")
public interface SpuFeign {
    /**
     * 新增Spu数据
     *
     * @param spu
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody Spu spu);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Long id);

    /**
     * 修改Spu数据
     *
     * @param spu
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody Spu spu, @PathVariable Long id);

    /**
     * 根据ID查询Spu数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<Spu> findById(@PathVariable Long id);

    /**
     * 查询Spu全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<Spu>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param spu
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<Spu>> findList(@RequestBody(required = false) Spu spu);

    /**
     * Spu分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Spu>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * Spu分页条件搜索实现
     *
     * @param spu
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Spu>> findPage(@RequestBody(required = false) Spu spu, @PathVariable int page, @PathVariable int size);

}