package com.changgou.goods.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @Author:admin
 * @Description:Album构建
 * @Date 2019/6/14 19:13
 */
@Data
@ApiModel(description = "Album", value = "Album")
@Table(name = "tb_album")
public class Album implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;// 编号

    @ApiModelProperty(value = "title", required = false)
    @Column(name = "title")
    private String title;// 相册名称

    @ApiModelProperty(value = "image", required = false)
    @Column(name = "image")
    private String image;// 相册封面

    @ApiModelProperty(value = "image_items", required = false)
    @Column(name = "image_items")
    private String imageItems;// 图片列表

}
