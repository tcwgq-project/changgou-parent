package com.changgou.goods.feign;

import com.changgou.common.response.Result;
import com.changgou.goods.pojo.Pref;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * PrefFeign
 *
 * @author tcwgq
 * @since 2022/05/29 20:23
 */
@FeignClient(name = "goods-service", contextId = "prefFeign")
@RequestMapping("/pref")
public interface PrefFeign {
    /**
     * 新增Pref数据
     *
     * @param pref
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody Pref pref);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Long id);

    /**
     * 修改Pref数据
     *
     * @param pref
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody Pref pref, @PathVariable Long id);

    /**
     * 根据ID查询Pref数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<Pref> findById(@PathVariable Long id);

    /**
     * 查询Pref全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<Pref>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param pref
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<Pref>> findList(@RequestBody(required = false) Pref pref);

    /**
     * Pref分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Pref>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * Pref分页条件搜索实现
     *
     * @param pref
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Pref>> findPage(@RequestBody(required = false) Pref pref, @PathVariable int page, @PathVariable int size);

}