package com.changgou.goods.feign;

import com.changgou.common.response.Result;
import com.changgou.goods.pojo.Para;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * ParaFeign
 *
 * @author tcwgq
 * @since 2022/05/29 20:23
 */
@FeignClient(name = "goods-service", contextId = "paraFeign")
@RequestMapping("/para")
public interface ParaFeign {
    /**
     * 新增Para数据
     *
     * @param para
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody Para para);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Long id);

    /**
     * 修改Para数据
     *
     * @param para
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody Para para, @PathVariable Long id);

    /**
     * 根据ID查询Para数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<Para> findById(@PathVariable Long id);

    /**
     * 查询Para全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<Para>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param para
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<Para>> findList(@RequestBody(required = false) Para para);

    /**
     * Para分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Para>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * Para分页条件搜索实现
     *
     * @param para
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Para>> findPage(@RequestBody(required = false) Para para, @PathVariable int page, @PathVariable int size);

}