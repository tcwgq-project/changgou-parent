package com.changgou.goods.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @Author:admin
 * @Description:CategoryBrand构建
 * @Date 2019/6/14 19:13
 */
@Data
@ApiModel(description = "CategoryBrand", value = "CategoryBrand")
@Table(name = "tb_category_brand")
public class CategoryBrand implements Serializable {
    @ApiModelProperty(value = "分类ID", required = false)
    @Id
    @Column(name = "category_id")
    private Long categoryId;// 分类ID

    @ApiModelProperty(value = "品牌ID", required = false)
    @Id
    @Column(name = "brand_id")
    private Long brandId;// 品牌ID

}
