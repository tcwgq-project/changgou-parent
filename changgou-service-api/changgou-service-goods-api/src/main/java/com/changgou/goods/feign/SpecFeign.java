package com.changgou.goods.feign;

import com.changgou.common.response.Result;
import com.changgou.goods.pojo.Spec;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * SpecFeign
 *
 * @author tcwgq
 * @since 2022/05/29 20:23
 */
@FeignClient(name = "goods-service", contextId = "specFeign")
@RequestMapping("/spec")
public interface SpecFeign {
    /**
     * 新增Spec数据
     *
     * @param spec
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody Spec spec);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Long id);

    /**
     * 修改Spec数据
     *
     * @param spec
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody Spec spec, @PathVariable Long id);

    /**
     * 根据ID查询Spec数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<Spec> findById(@PathVariable Long id);

    /**
     * 查询Spec全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<Spec>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param spec
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<Spec>> findList(@RequestBody(required = false) Spec spec);

    /**
     * Spec分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Spec>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * Spec分页条件搜索实现
     *
     * @param spec
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Spec>> findPage(@RequestBody(required = false) Spec spec, @PathVariable int page, @PathVariable int size);

}