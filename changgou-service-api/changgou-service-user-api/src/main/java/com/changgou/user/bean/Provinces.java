package com.changgou.user.bean;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * ProvincesBean
 *
 * @author tcwgq
 * @since 2022/06/12 21:57
 */
@Data
@Table(name = "tb_provinces")
public class Provinces implements Serializable {
    @Id
    @Column(name = "provinceid")
    private String provinceid;// 省份ID

    @Column(name = "province")
    private String province;// 省份名称

}
