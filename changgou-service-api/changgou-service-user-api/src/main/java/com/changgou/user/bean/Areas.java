package com.changgou.user.bean;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * AreasBean
 *
 * @author tcwgq
 * @since 2022/06/12 21:57
 */
@Data
@Table(name = "tb_areas")
public class Areas implements Serializable {
    @Id
    @Column(name = "areaid")
    private String areaid;// 区域ID

    @Column(name = "area")
    private String area;// 区域名称

    @Column(name = "cityid")
    private String cityid;// 城市ID

}
