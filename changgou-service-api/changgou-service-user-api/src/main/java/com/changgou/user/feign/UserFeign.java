package com.changgou.user.feign;

import com.changgou.common.response.Result;
import com.changgou.user.bean.User;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * UserFeign
 *
 * @author tcwgq
 * @since 2022/06/12 21:57
 */
@FeignClient(name = "user-service", contextId = "userFeign")
@RequestMapping("/user")
public interface UserFeign {
    /**
     * 添加积分
     *
     * @param points
     * @param username
     * @return
     */
    @PostMapping("/points/add")
    Result<String> addPoints(@RequestParam(value = "points") Integer points,
                             @RequestParam(value = "username") String username);

    /**
     * 新增User数据
     *
     * @param user
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody User user);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable String id);

    /**
     * 修改User数据
     *
     * @param user
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody User user, @PathVariable String id);

    /**
     * 根据ID查询User数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<User> findById(@PathVariable String id);

    /**
     * 根据ID查询User数据，注意，findById与loadById路径不要合在一个方法里写
     *
     * @param id
     * @return
     */
    @GetMapping({"/load/{id}"})
    Result<User> loadById(@PathVariable String id);

    /**
     * 查询User全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<User>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param user
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<User>> findList(@RequestBody(required = false) User user);

    /**
     * User分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<User>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * User分页条件搜索实现
     *
     * @param user
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<User>> findPage(@RequestBody(required = false) User user, @PathVariable int page, @PathVariable int size);

}