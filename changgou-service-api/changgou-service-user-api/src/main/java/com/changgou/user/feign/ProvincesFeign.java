package com.changgou.user.feign;

import com.changgou.common.response.Result;
import com.changgou.user.bean.Provinces;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * ProvincesFeign
 *
 * @author tcwgq
 * @since 2022/06/12 21:57
 */
@FeignClient(name = "user-service", contextId = "provincesFeign")
@RequestMapping("/provinces")
public interface ProvincesFeign {
    /**
     * 新增Provinces数据
     *
     * @param provinces
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody Provinces provinces);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable String id);

    /**
     * 修改Provinces数据
     *
     * @param provinces
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody Provinces provinces, @PathVariable String id);

    /**
     * 根据ID查询Provinces数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<Provinces> findById(@PathVariable String id);

    /**
     * 查询Provinces全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<Provinces>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param provinces
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<Provinces>> findList(@RequestBody(required = false) Provinces provinces);

    /**
     * Provinces分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Provinces>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * Provinces分页条件搜索实现
     *
     * @param provinces
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Provinces>> findPage(@RequestBody(required = false) Provinces provinces, @PathVariable int page, @PathVariable int size);

}