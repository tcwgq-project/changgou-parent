package com.changgou.user.feign;

import com.changgou.common.response.Result;
import com.changgou.user.bean.UndoLog;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * UndoLogFeign
 *
 * @author tcwgq
 * @since 2022/06/12 21:57
 */
@FeignClient(name = "user-service", contextId = "userUndoLogFeign")
@RequestMapping("/user/undoLog")
public interface UndoLogFeign {
    /**
     * 新增UndoLog数据
     *
     * @param undoLog
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody UndoLog undoLog);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Long id);

    /**
     * 修改UndoLog数据
     *
     * @param undoLog
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody UndoLog undoLog, @PathVariable Long id);

    /**
     * 根据ID查询UndoLog数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<UndoLog> findById(@PathVariable Long id);

    /**
     * 查询UndoLog全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<UndoLog>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param undoLog
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<UndoLog>> findList(@RequestBody(required = false) UndoLog undoLog);

    /**
     * UndoLog分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<UndoLog>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * UndoLog分页条件搜索实现
     *
     * @param undoLog
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<UndoLog>> findPage(@RequestBody(required = false) UndoLog undoLog, @PathVariable int page, @PathVariable int size);

}