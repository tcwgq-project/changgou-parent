package com.changgou.user.feign;

import com.changgou.common.response.Result;
import com.changgou.user.bean.Address;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * AddressFeign
 *
 * @author tcwgq
 * @since 2022/06/12 21:57
 */
@FeignClient(name = "user-service", contextId = "addressFeign")
@RequestMapping("/address")
public interface AddressFeign {
    /**
     * 新增Address数据
     *
     * @param address
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody Address address);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Integer id);

    /**
     * 修改Address数据
     *
     * @param address
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody Address address, @PathVariable Integer id);

    /**
     * 根据ID查询Address数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<Address> findById(@PathVariable Integer id);

    @GetMapping("/user/list")
    Result<List<Address>> findByUsername();

    /**
     * 查询Address全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<Address>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param address
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<Address>> findList(@RequestBody(required = false) Address address);

    /**
     * Address分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Address>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * Address分页条件搜索实现
     *
     * @param address
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Address>> findPage(@RequestBody(required = false) Address address, @PathVariable int page, @PathVariable int size);

}