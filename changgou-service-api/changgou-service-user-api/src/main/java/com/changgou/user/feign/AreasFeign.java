package com.changgou.user.feign;

import com.changgou.common.response.Result;
import com.changgou.user.bean.Areas;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * AreasFeign
 *
 * @author tcwgq
 * @since 2022/06/12 21:57
 */
@FeignClient(name = "user-service", contextId = "areasFeign")
@RequestMapping("/areas")
public interface AreasFeign {
    /**
     * 新增Areas数据
     *
     * @param areas
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody Areas areas);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable String id);

    /**
     * 修改Areas数据
     *
     * @param areas
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody Areas areas, @PathVariable String id);

    /**
     * 根据ID查询Areas数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<Areas> findById(@PathVariable String id);

    /**
     * 查询Areas全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<Areas>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param areas
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<Areas>> findList(@RequestBody(required = false) Areas areas);

    /**
     * Areas分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Areas>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * Areas分页条件搜索实现
     *
     * @param areas
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Areas>> findPage(@RequestBody(required = false) Areas areas, @PathVariable int page, @PathVariable int size);

}