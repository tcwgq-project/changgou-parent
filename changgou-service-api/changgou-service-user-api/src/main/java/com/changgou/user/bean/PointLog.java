package com.changgou.user.bean;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * PointLogBean
 *
 * @author tcwgq
 * @since 2022/06/25 10:01
 */
@Data
@Table(name = "tb_point_log")
public class PointLog implements Serializable {
    @Id
    @Column(name = "order_id")
    private String orderId;//

    @Column(name = "user_id")
    private String userId;//

    @Column(name = "point")
    private Integer point;//

}
