package com.changgou.user.feign;

import com.changgou.common.response.Result;
import com.changgou.user.bean.Cities;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * CitiesFeign
 *
 * @author tcwgq
 * @since 2022/06/12 21:57
 */
@FeignClient(name = "user-service", contextId = "citiesFeign")
@RequestMapping("/cities")
public interface CitiesFeign {
    /**
     * 新增Cities数据
     *
     * @param cities
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody Cities cities);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable String id);

    /**
     * 修改Cities数据
     *
     * @param cities
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody Cities cities, @PathVariable String id);

    /**
     * 根据ID查询Cities数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<Cities> findById(@PathVariable String id);

    /**
     * 查询Cities全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<Cities>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param cities
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<Cities>> findList(@RequestBody(required = false) Cities cities);

    /**
     * Cities分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Cities>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * Cities分页条件搜索实现
     *
     * @param cities
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Cities>> findPage(@RequestBody(required = false) Cities cities, @PathVariable int page, @PathVariable int size);

}