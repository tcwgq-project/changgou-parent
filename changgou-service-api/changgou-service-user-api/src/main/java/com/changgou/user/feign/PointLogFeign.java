package com.changgou.user.feign;

import com.changgou.common.response.Result;
import com.changgou.user.bean.PointLog;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * PointLogFeign
 *
 * @author tcwgq
 * @since 2022/06/25 10:01
 */
@FeignClient(name = "user-service", contextId = "pointLogFeign")
@RequestMapping("/pointLog")
public interface PointLogFeign {
    /**
     * 新增PointLog数据
     *
     * @param pointLog
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody PointLog pointLog);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable String id);

    /**
     * 修改PointLog数据
     *
     * @param pointLog
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody PointLog pointLog, @PathVariable String id);

    /**
     * 根据ID查询PointLog数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<PointLog> findById(@PathVariable String id);

    /**
     * 查询PointLog全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<PointLog>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param pointLog
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<PointLog>> findList(@RequestBody(required = false) PointLog pointLog);

    /**
     * PointLog分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<PointLog>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * PointLog分页条件搜索实现
     *
     * @param pointLog
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<PointLog>> findPage(@RequestBody(required = false) PointLog pointLog, @PathVariable int page, @PathVariable int size);

}