package com.changgou.content.feign;

import com.changgou.common.response.Result;
import com.changgou.content.pojo.Content;
import com.github.pagehelper.PageInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * ContentFeign
 *
 * @author tcwgq
 * @since 2022/05/29 20:00
 */
@FeignClient(name = "content-service", contextId = "contentFeign")
@RequestMapping("/content")
public interface ContentFeign {
    /**
     * 新增Content数据
     *
     * @param content
     * @return
     */
    @PostMapping
    Result<String> add(@RequestBody Content content);

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    Result<String> delete(@PathVariable Long id);

    /**
     * 修改Content数据
     *
     * @param content
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    Result<String> update(@RequestBody Content content, @PathVariable Long id);

    /**
     * 根据ID查询Content数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    Result<Content> findById(@PathVariable Long id);

    /**
     * 根据分类的ID 获取到广告列表
     */
    @GetMapping(value = "/list/category/{id}")
    Result<List<Content>> findByCategory(@PathVariable(name = "id") Long id);

    /**
     * 查询Content全部数据
     *
     * @return
     */
    @GetMapping
    Result<List<Content>> findAll();

    /**
     * 多条件搜索品牌数据
     *
     * @param content
     * @return
     */
    @PostMapping(value = "/search")
    Result<List<Content>> findList(@RequestBody(required = false) Content content);

    /**
     * Content分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Content>> findPage(@PathVariable int page, @PathVariable int size);

    /**
     * Content分页条件搜索实现
     *
     * @param content
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    Result<PageInfo<Content>> findPage(@RequestBody(required = false) Content content, @PathVariable int page, @PathVariable int size);

}