package com.changgou.common;

import io.jsonwebtoken.*;
import org.junit.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author tcwgq
 * @since 2022/6/13 8:45
 */
public class JwtTest {
    @Test
    public void test1() {
        JwtBuilder builder = Jwts.builder();
        builder.setId("xaga");
        builder.setIssuer("tcwgq");// 作者
        builder.setIssuedAt(new Date());// 签发日期

        builder.setHeader(new HashMap<>());
        builder.setHeaderParams(new HashMap<>());

        Map<String, Object> map = new HashMap<>();
        map.put("username", "zhangSan");
        map.put("age", 23);
        map.put("address", "上海");
        // Both 'payload' and 'claims' cannot both be specified. Choose either one.
        // builder.setPayload("{}");
        // builder.setClaims(new HashMap<>());
        builder.addClaims(map);

        builder.setSubject("helloworld");
        builder.setAudience("you");
        builder.setExpiration(new Date(System.currentTimeMillis() + 3600000));// 过期时间
        // builder.setNotBefore();
        builder.signWith(SignatureAlgorithm.HS256, "hello");
        String compact = builder.compact();
        System.out.println(compact);
    }

    @Test
    public void test2() {
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJ4YWdhIiwiaXNzIjoidGN3Z3EiLCJpYXQiOjE2NTUwODI2MzcsInN1YiI6ImhlbGxvd29ybGQiLCJhdWQiOiJ5b3UiLCJleHAiOjE2NTUwODI2NDd9.NhB2gVE3RntAKcFAZKQnrogpwBsOCX7zGudojnTTxPk";
        Jwt hello = Jwts.parser().setSigningKey("hello").parse(token);
        Object body = hello.getBody();
        System.out.println(body);
    }

    @Test
    public void test3() {
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJ4YWdhIiwiaXNzIjoidGN3Z3EiLCJpYXQiOjE2NTUwODI4MTEsImFkZHJlc3MiOiLkuIrmtbciLCJhZ2UiOjIzLCJ1c2VybmFtZSI6InpoYW5nU2FuIiwic3ViIjoiaGVsbG93b3JsZCIsImF1ZCI6InlvdSIsImV4cCI6MTY1NTA4NjQxMX0.Br79ZZAXWmDXdexBHjdgCe1LsUTVmEHiRvDyGXfXdmI";
        Claims claims = Jwts.parser().setSigningKey("hello").parseClaimsJws(token).getBody();
        System.out.println(claims);
    }

}
