package com.changgou.common.errorcode;

import java.util.Objects;

/**
 * 通用服务错误码为4位，各含义如注释
 * SUCCESS为特殊错误码，只占1位
 *
 * @author tcwgq
 * @time 2020/9/14 21:33
 */
public class BaseErrorCode extends RuntimeException implements ErrorCode {
    /**
     * 操作成功
     */
    public static ErrorCode SUCCESS = new BaseErrorCode(0, "OK");

    /**
     * 通用的服务内部错误，如发生意外异常、非代码逻辑运行时异常、网络异常等等造成的程序错误可以使用该值统一标示
     */
    public static ErrorCode SERVER_ERROR = new BaseErrorCode(1000, "服务内部错误");

    /**
     * 通用的应用逻辑错误码，对于那些没有严格具体业务规则错误码的业务场景可以使用该错误码统一标示错误
     */
    public static ErrorCode APPLICATION_ERROR = new BaseErrorCode(2000, "应用逻辑错误");

    /**
     * 通用的入参错误码，对于某些参数没有制定具体的错误码时可以采用该值，但错误信息中要尽量描述是什么参数错误导致
     */
    public static ErrorCode PARAMETER_ERROR = new BaseErrorCode(3000, "参数错误");

    /**
     * 服务收到了请求但是拒绝服务
     */
    public static ErrorCode ACCESS_FORBIDDEN = new BaseErrorCode(4000, "访问受限");

    /**
     * 访问太频繁了
     */
    public static ErrorCode ACCESS_TOO_FREQUENTLY = new BaseErrorCode(5000, "访问太频繁");

    /**
     * 没有权限
     */
    public static ErrorCode NO_PRIVILEGE = new BaseErrorCode(6000, "没有权限");

    private final Integer code;
    private final String message;

    public BaseErrorCode(Integer code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public BaseErrorCode(Integer appId, Integer tb, Integer no, String message) {
        this(appId * 100000 + tb * 100 + no, message);
    }

    public BaseErrorCode(Integer appId, Integer type, Integer businessId, Integer no, String message) {
        this(appId * 100000 + type * 1000 + businessId * 100 + no, message);
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseErrorCode that = (BaseErrorCode) o;
        return Objects.equals(code, that.code) &&
                Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, message);
    }

    @Override
    public String toString() {
        return "BaseErrorCode{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }

}
