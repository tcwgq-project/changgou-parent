package com.changgou.common.utils;

import org.joda.time.*;
import org.joda.time.format.DateTimeFormat;

import java.util.Date;

/**
 * joda time工具类
 *
 * @author tcwgq on 2019/11/8 14:28
 */
public class DateUtils {
    public static final String SEC_FULL_PATTERN = "yyyy-MM-dd HH:mm:ss";
    public static final String MICRO_FULL_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String CHS_FULL_PATTERN = "yyyy年MM月dd日 HH时mm分ss秒";
    public static final String SLASH_FULL_PATTERN = "yyyy/MM/dd HH/mm/ss";
    public static final String POINT_FULL_PATTERN = "yyyy.MM.dd HH/mm/ss";
    public static final String YMD_HALF_PATTERN = "yyyy-MM-dd";
    public static final String HMS_HALF_PATTERN = "HH:mm:ss";

    /**
     * 当前时间毫秒值
     *
     * @return 当前时间毫秒值
     */
    public static long currentMills() {
        return now().getMillis();
    }

    /**
     * 当前分钟秒值
     *
     * @return 当前分钟秒值，0-59
     */
    public static int currentSeconds() {
        return now().getSecondOfMinute();
    }

    /**
     * 当前小时分钟值
     *
     * @return 当前小时分钟值，0-59
     */
    public static int currentMinutes() {
        return now().getMinuteOfHour();
    }

    /**
     * 当前日期开始时间
     *
     * @return
     */
    public static Date startOfDay() {
        return now().withTimeAtStartOfDay().toDate();
    }

    /**
     * 当前日期结束时间
     *
     * @return
     */
    public static Date endOfDay() {
        return now().hourOfDay().withMaximumValue().
                minuteOfHour().withMaximumValue().
                secondOfMinute().withMaximumValue().
                millisOfSecond().withMaximumValue().toDate();
        // return now().
        //        withHourOfDay(23).
        //        withMinuteOfHour(23).
        //        withSecondOfMinute(59).
        //        withMillisOfSecond(999).toDate();

    }

    /**
     * 指定日期开始时间
     *
     * @return
     */
    public static Date startOfDay(Date date) {
        return toDateTime(date).withTimeAtStartOfDay().toDate();
    }

    /**
     * 指定日期结束时间
     *
     * @return
     */
    public static Date endOfDay(Date date) {
        return toDateTime(date).hourOfDay().withMaximumValue().
                minuteOfHour().withMaximumValue().
                secondOfMinute().withMaximumValue().
                millisOfSecond().withMaximumValue().toDate();
    }

    /**
     * 指定日期所在月份的月初当天午夜时间
     *
     * @param date 指定日期
     * @return
     */
    public static Date startOfMonth(Date date) {
        return toDateTime(date).dayOfMonth().withMinimumValue().withTimeAtStartOfDay().toDate();
    }

    /**
     * 指定日期所在月份的月末当天凌晨时间
     *
     * @param date 指定日期
     * @return
     */
    public static Date endOfMonth(Date date) {
        return toDateTime(date).dayOfMonth().withMaximumValue().withTimeAtStartOfDay().toDate();
    }

    /**
     * 当前日期开始时间字符串
     *
     * @param pattern 格式串
     * @return
     */
    public static String startOfDay(String pattern) {
        return format(startOfDay(), pattern);
    }

    /**
     * 当前日期结束时间字符串
     *
     * @param pattern 格式串
     * @return
     */
    public static String endOfDay(String pattern) {
        return format(endOfDay(), pattern);
    }

    /**
     * 距离当前日期前后days的日期时间
     *
     * @param date 指定日期
     * @param days 日期偏移量
     * @return
     */
    public static Date daysAfter(Date date, int days) {
        return toDateTime(date).plusDays(days).toDate();
    }

    /**
     * 指定两日期的时间差值，单位：毫秒
     *
     * @param start 开始时间
     * @param end   结束时间
     * @return
     */
    public static long betweenMills(Date start, Date end) {
        /**
         * 此种方式报java.lang.ArithmeticException: Value cannot fit in an int: 5561540597
         */
        // return toPeriod(toDateTime(start), toDateTime(end), PeriodType.millis()).getMillis();
        return new Duration(toDateTime(start), toDateTime(end)).getMillis();
        // 包左不包右
        // return new Interval(toDateTime(start), toDateTime(end)).toDurationMillis();
    }

    /**
     * 指定两日期的时间差值，单位：秒
     *
     * @param start 开始时间
     * @param end   结束时间
     * @return
     */
    public static int betweenSeconds(Date start, Date end) {
        return toPeriod(toDateTime(start), toDateTime(end), PeriodType.seconds()).getSeconds();
    }

    /**
     * 指定两日期的时间差值，单位：分
     *
     * @param start 开始时间
     * @param end   结束时间
     * @return
     */
    public static int betweenMinutes(Date start, Date end) {
        return toPeriod(toDateTime(start), toDateTime(end), PeriodType.minutes()).getMinutes();
    }

    /**
     * 指定两日期的时间差值，单位：分
     *
     * @param start 开始时间
     * @param end   结束时间
     * @return
     */
    public static int betweenHours(Date start, Date end) {
        return toPeriod(toDateTime(start), toDateTime(end), PeriodType.hours()).getHours();
    }

    /**
     * 指定两日期的时间差值，单位：天
     *
     * @param start 开始时间
     * @param end   结束时间
     * @return
     */
    public static int betweenDays(Date start, Date end) {
        return toPeriod(toDateTime(start), toDateTime(end), PeriodType.days()).getDays();
    }

    /**
     * 指定两日期的时间差值，单位：月
     *
     * @param start 开始时间
     * @param end   结束时间
     * @return
     */
    public static int betweenMonths(Date start, Date end) {
        return toPeriod(toDateTime(start), toDateTime(end), PeriodType.months()).getMonths();
    }

    /**
     * 指定两日期的时间差值，单位：年
     *
     * @param start 开始时间
     * @param end   结束时间
     * @return
     */
    public static int betweenYears(Date start, Date end) {
        return toPeriod(toDateTime(start), toDateTime(end), PeriodType.years()).getYears();
    }

    /**
     * 距离当前日期前后days的日期时间
     *
     * @param date 指定日期
     * @param days 日期偏移量
     * @return
     */
    public static Date startOfDaysAfter(Date date, int days) {
        return toDateTime(date).plusDays(days).withTimeAtStartOfDay().toDate();
    }

    /**
     * 距离当前日期前后days的日期时间
     *
     * @param date 指定日期
     * @param days 日期偏移量
     * @return
     */
    public static Date endOfDaysAfter(Date date, int days) {
        return toDateTime(date).
                plusDays(days).
                withHourOfDay(23).
                withMinuteOfHour(59).
                withSecondOfMinute(59).
                withMillisOfSecond(999).toDate();
    }

    /**
     * 日期格式化
     *
     * @param date    java.util.Date
     * @param pattern 日期格式
     * @return 日期字符串
     */
    public static String format(Date date, String pattern) {
        return format(toDateTime(date), pattern);
    }

    /**
     * 日期格式化
     *
     * @param dateTime org.joda.time.DateTime
     * @param pattern  日期格式
     * @return 日期字符串
     */
    public static String format(DateTime dateTime, String pattern) {
        return dateTime.toString(pattern);
    }

    /**
     * 根据指定格式将日期字符串解析为东八区日期时间
     *
     * @param date    日期字符串
     * @param pattern 格式字符串
     * @return
     */
    public static Date parse(String date, String pattern) {
        return DateTime.parse(date, DateTimeFormat.forPattern(pattern).withZone(DateTimeZone.forOffsetHours(8))).toDate();
    }

    /**
     * 获取东八区当前时间
     *
     * @return
     */
    private static DateTime now() {
        // 东八区时间
        return DateTime.now(DateTimeZone.forOffsetHours(8));
    }

    /**
     * @param date 指定日期转为东八区时间
     * @return
     */
    private static DateTime toDateTime(Date date) {
        // 东八区时间
        return new DateTime(date, DateTimeZone.forOffsetHours(8));
    }

    /**
     * 标准区间类型
     *
     * @param startInstant 开始时间
     * @param endInstant   结束时间
     * @return
     */
    private static Period toPeriod(ReadableInstant startInstant, ReadableInstant endInstant, PeriodType periodType) {
        return new Period(startInstant, endInstant, periodType);
    }

}
