package com.changgou.common.utils;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author tcwgq 2019/5/30 17:08
 */
public class OrikaUtil {
    private static final MapperFacade mapperFacade;

    static {
        mapperFacade = InstancePlaceHolder.MAPPER_FACTORY.getMapperFacade();
    }

    static class InstancePlaceHolder {
        final static MapperFactory MAPPER_FACTORY = new DefaultMapperFactory.Builder().build();
    }

    public static <S, D> D map(S source, Class<D> clazz) {
        Objects.requireNonNull(clazz, "map clazz can't be null !");

        return mapperFacade.map(source, clazz);
    }

    public static <S, D> List<D> mapAsList(Iterable<S> source, Class<D> clazz) {
        Objects.requireNonNull(clazz, "map clazz can't be null !");

        List<D> ds = mapperFacade.mapAsList(source, clazz);

        if (ds == null) {
            return Collections.emptyList();
        }

        return ds;
    }

    public static <S, D> void map(S source, D dest) {
        Objects.requireNonNull(source, "source can't be null !");
        Objects.requireNonNull(dest, "dest can't be null !");

        mapperFacade.map(source, dest);
    }

}
