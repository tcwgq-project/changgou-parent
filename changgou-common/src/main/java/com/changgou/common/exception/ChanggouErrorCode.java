package com.changgou.common.exception;

import com.changgou.common.errorcode.BaseErrorCode;
import com.changgou.common.errorcode.ErrorCode;

/**
 * @author tcwgq
 * @since 2022/6/25 11:32
 */
public class ChanggouErrorCode extends BaseErrorCode {
    private static final int APP_ID = 20000;

    public static ErrorCode DECREASE_GOOD_NUM_FAIL = new ChanggouErrorCode(113, 12, "减少库存失败");
    public static ErrorCode GOOD_NOT_EXIST = new ChanggouErrorCode(113, 12, "商品不存在");
    public static ErrorCode GOOD_NOT_EXIST_OR_DELETE = new ChanggouErrorCode(113, 12, "商品不存在或者已经删除");
    public static ErrorCode GOOD_ON_SAIL = new ChanggouErrorCode(113, 12, "商品还没下架，不能删除");
    public static ErrorCode GOOD_LACK = new ChanggouErrorCode(113, 12, "商品没有库存，下单失败");
    public static ErrorCode GOOD_SOLD_OUT = new ChanggouErrorCode(113, 12, "卖完了");
    public static ErrorCode GOOD_MUST_ON = new ChanggouErrorCode(113, 12, "商品必须要审核或者商品必须要是上架的状态");
    public static ErrorCode GOOD_MUST_ON1 = new ChanggouErrorCode(113, 12, "商品必须要审核或者商品必须要是未上架的状态");

    public static ErrorCode USER_QUEUE_DUPLICATE = new ChanggouErrorCode(113, 12, "用户已经排过队，不能重复排队");

    public static ErrorCode ORDER_NOT_EXIST = new ChanggouErrorCode(114, 13, "订单不存在");
    public static ErrorCode ORDER_UN_SEND = new ChanggouErrorCode(114, 13, "订单未发货");
    public static ErrorCode ORDER_NO_EMPTY = new ChanggouErrorCode(114, 13, "订单号为空");
    public static ErrorCode ORDER_NO_WRONG = new ChanggouErrorCode(114, 13, "订单状态有误");
    public static ErrorCode CHOSE_DELIVERY = new ChanggouErrorCode(114, 13, "请选择快递公司和填写快递单号");

    public static ErrorCode USELESS_REQUEST = new ChanggouErrorCode(110, 14, "无效访问");
    public static ErrorCode CAN_FIND_SERVICE = new ChanggouErrorCode(110, 14, "找不到对应的服务");
    public static ErrorCode CREATE_TOKEN_FAIL = new ChanggouErrorCode(110, 14, "创建令牌失败");

    public static ErrorCode CREATE_TOKEN_RESPONSE_FAIL = new ChanggouErrorCode(110, 14, "申请令牌响应异常");


    public ChanggouErrorCode(Integer code, String msg) {
        super(code, msg);
    }

    public ChanggouErrorCode(Integer tb, Integer no, String msg) {
        super(APP_ID, tb, no, msg);
    }

    public ChanggouErrorCode(Integer type, Integer businessId, Integer no, String msg) {
        super(APP_ID, type, businessId, no, msg);
    }

    public ChanggouErrorCode(Integer appId, Integer type, Integer businessId, Integer no, String msg) {
        super(appId, type, businessId, no, msg);
    }

}
