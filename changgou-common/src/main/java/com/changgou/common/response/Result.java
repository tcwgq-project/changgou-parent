package com.changgou.common.response;

import com.changgou.common.entity.StatusCode;
import com.changgou.common.errorcode.ErrorCode;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 三国的包子
 */
@Data
public class Result<T> implements Serializable {
    private static final String success = "success!";
    @ApiModelProperty(value = "是否成功")
    private Boolean flag;
    @ApiModelProperty(value = "返回码")
    private Integer code;
    @ApiModelProperty(value = "返回消息")
    private String message;
    @ApiModelProperty(value = "返回数据")
    private T data;

    public Result() {
        this.flag = true;
        this.code = StatusCode.OK;
        this.message = success;
    }

    public Result(Boolean flag, Integer code, String message) {
        this.flag = flag;
        this.code = code;
        this.message = message;
    }

    public Result(Boolean flag, Integer code, String message, T data) {
        this.flag = flag;
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static <T> Result<T> success(T data) {
        return new Result<>(true, StatusCode.OK, success, data);
    }

    public static <T> Result<T> success(Integer code, String message, T data) {
        return new Result<>(true, code, message, data);
    }

    public static <T> Result<T> success(Integer code, String message) {
        return new Result<>(true, code, message);
    }

    public static <T> Result<T> success(ErrorCode errorCode, T data) {
        return new Result<>(true, errorCode.getCode(), errorCode.getMessage(), data);
    }

    public static <T> Result<T> fail(Integer code, String message) {
        return new Result<>(true, code, message);
    }

    public static <T> Result<T> fail(ErrorCode errorCode) {
        return new Result<>(true, errorCode.getCode(), errorCode.getMessage());
    }

}
