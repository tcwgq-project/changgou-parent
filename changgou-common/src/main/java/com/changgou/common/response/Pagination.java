package com.changgou.common.response;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * 接口层分页模型对象
 */
public class Pagination<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final int DEFAULT_PAGE_NUM = 1;
    public static final int DEFAULT_PAGE_SIZE = 20;
    public static final int MAX_PAGE_SIZE = 200;

    /**
     * <strong>属性名不要更改</strong>
     */
    @ApiModelProperty(value = "总记录数")
    private int total;

    /**
     * <strong>属性名不要更改</strong>
     */
    @ApiModelProperty(value = "数据列表")
    private List<T> dataList;

    public Pagination() {

    }

    public Pagination(int total, List<T> dataList) {
        this.total = total;
        this.dataList = dataList;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<T> getDataList() {
        return dataList;
    }

    public void setDataList(List<T> dataList) {
        this.dataList = dataList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pagination)) return false;

        Pagination<?> that = (Pagination<?>) o;

        if (total != that.total) return false;
        return dataList != null ? dataList.equals(that.dataList) : that.dataList == null;
    }

    @Override
    public int hashCode() {
        int result = total;
        result = 31 * result + (dataList != null ? dataList.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Pagination{" +
                "totalCount=" + total +
                ", dataList=" + dataList +
                '}';
    }
}
