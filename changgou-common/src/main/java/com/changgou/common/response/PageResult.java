package com.changgou.common.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 分页结果类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageResult<T> {
    @ApiModelProperty(value = "总记录数")
    private Long total;// 总记录数

    @ApiModelProperty(value = "记录")
    private List<T> rows;// 记录

}
