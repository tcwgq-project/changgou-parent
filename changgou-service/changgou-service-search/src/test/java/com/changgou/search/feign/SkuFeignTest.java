package com.changgou.search.feign;

import com.changgou.common.response.Result;
import com.changgou.goods.feign.SkuFeign;
import com.changgou.goods.pojo.Sku;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author tcwgq
 * @since 2022/6/8 19:49
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class SkuFeignTest {
    @Autowired
    private SkuFeign skuFeign;

    @Test
    public void add() {
    }

    @Test
    public void delete() {
    }

    @Test
    public void update() {
    }

    @Test
    public void findById() {
    }

    @Test
    public void findAll() {
    }

    @Test
    public void findList() {
        Sku sku = new Sku();
        sku.setStatus("1");
        Result<List<Sku>> skuResult = skuFeign.findList(sku);
        List<Sku> data = skuResult.getData();
        System.out.println(data);
    }

    @Test
    public void findPage() {
    }

    @Test
    public void testFindPage() {
    }
}