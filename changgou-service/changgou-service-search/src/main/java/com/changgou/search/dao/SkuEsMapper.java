package com.changgou.search.dao;

import com.changgou.search.bean.SkuInfo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * 描述
 *
 * @version 1.0
 * @since 1.0
 */
public interface SkuEsMapper extends ElasticsearchRepository<SkuInfo, Long> {

}
