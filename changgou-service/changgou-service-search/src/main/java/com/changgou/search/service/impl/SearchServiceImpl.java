package com.changgou.search.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.changgou.common.response.Result;
import com.changgou.goods.feign.SkuFeign;
import com.changgou.goods.pojo.Sku;
import com.changgou.search.bean.SkuInfo;
import com.changgou.search.dao.SkuEsMapper;
import com.changgou.search.service.SearchService;
import org.apache.commons.lang.StringUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.SearchResultMapper;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 描述
 *
 * @version 1.0
 * @since 1.0
 */
@Service
public class SearchServiceImpl implements SearchService {
    @Autowired
    private SkuFeign skuFeign;

    @Autowired
    private SkuEsMapper skuEsMapper;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Override
    public void importEs() {
        // 1.调用 goods微服务的fegin 查询 符合条件的sku的数据
        Sku sku = new Sku();
        sku.setStatus("1");
        Result<List<Sku>> skuResult = skuFeign.findList(sku);
        List<Sku> data = skuResult.getData();
        // 2.将sku的列表 转换成es中的skuinfo的列表
        List<SkuInfo> skuInfos = JSON.parseArray(JSON.toJSONString(data), SkuInfo.class);
        for (SkuInfo skuInfo : skuInfos) {
            // 获取规格的数据 {"电视音响效果":"立体声","电视屏幕尺寸":"20英寸","尺码":"165"}
            // 转成MAP  key: 规格的名称  value:规格的选项的值
            Map<String, Object> map = JSON.parseObject(skuInfo.getSpec(), new TypeReference<Map<String, Object>>() {
            });
            skuInfo.setSpecMap(map);
        }

        // 3.调用spring data elasticsearch的API 导入到ES中
        skuEsMapper.saveAll(skuInfos);
    }

    @Override
    public Map<String, Object> search(Map<String, String> map) {
        NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        if (map != null && map.size() > 0) {
            // 关键字查询
            String keywords = map.get("keywords");
            if (StringUtils.isNotEmpty(keywords)) {
                // builder.withQuery(QueryBuilders.queryStringQuery(keywords).field("name"));
                boolQueryBuilder.must(QueryBuilders.queryStringQuery(keywords).field("name"));
                // 添加高亮
                HighlightBuilder.Field field = new HighlightBuilder.Field("name");
                // 前缀 <em style="color:red;">
                field.preTags("<em style=\"color:red;\">");
                // 后缀</em>
                field.postTags("</em>");
                // 碎片长度
                field.fragmentSize(100);
                nativeSearchQueryBuilder.withHighlightFields(field);
            }

            // 类别筛选
            if (StringUtils.isNotEmpty(map.get("category"))) {
                boolQueryBuilder.must(QueryBuilders.termQuery("categoryName", map.get("category")));
            }

            // 品牌筛选
            if (StringUtils.isNotEmpty(map.get("brand"))) {
                boolQueryBuilder.must(QueryBuilders.termQuery("brandName", map.get("brand")));
            }

            // 价格筛选
            String price = map.get("price");
            if (StringUtils.isNotEmpty(price)) {
                price = price.replace("元", "").replace("以上", "");
                String[] split = price.split("-");
                if (split.length > 0) {
                    boolQueryBuilder.must(QueryBuilders.rangeQuery("price").gte(Integer.parseInt(split[0])));
                    if (split.length == 2) {
                        boolQueryBuilder.must(QueryBuilders.rangeQuery("price").lte(Integer.parseInt(split[1])));
                    }
                }
            }

            // spec筛选
            for (String key : map.keySet()) {
                //{ brand:"",category:"",spec_网络:"电信4G"}
                if (key.startsWith("spec_")) {
                    // 截取规格的名称
                    boolQueryBuilder.filter(QueryBuilders.termQuery("specMap." + key.substring(5) + ".keyword", map.get(key)));
                }
            }

            // 排序
            String sortField = map.get("sortField");
            String sortRule = map.get("sortRule");
            if (StringUtils.isNotEmpty(sortField) && StringUtils.isNotEmpty(sortRule)) {
                nativeSearchQueryBuilder.withSort(new FieldSortBuilder(sortField).order(SortOrder.valueOf(sortRule)));
            }
        }
        nativeSearchQueryBuilder.withQuery(boolQueryBuilder);

        // 分页
        String pageNo = "1";
        String pageSize = "10";
        if (map != null && StringUtils.isNotEmpty(map.get("pageNo"))) {
            pageNo = map.get("pageNo");
        }
        if (map != null && StringUtils.isNotEmpty(map.get("pageSize"))) {
            pageSize = map.get("pageSize");
        }
        int i = Integer.parseInt(pageSize);
        if (i > 5000) {
            i = 5000;
        }
        nativeSearchQueryBuilder.withPageable(PageRequest.of(Integer.parseInt(pageNo) - 1, i));

        // 分组查询
        // skuCategory为别名
        if (map == null || StringUtils.isEmpty(map.get("category"))) {
            group("skuCategory", "categoryName", nativeSearchQueryBuilder);
        }
        if (map == null || StringUtils.isEmpty(map.get("brand"))) {
            group("skuBrand", "brandName", nativeSearchQueryBuilder);
        }

        group("skuSpec", "spec.keyword", nativeSearchQueryBuilder);

        // AggregatedPage<SkuInfo> page = elasticsearchTemplate.queryForPage(builder.build(), SkuInfo.class);
        AggregatedPage<SkuInfo> page = elasticsearchTemplate.queryForPage(nativeSearchQueryBuilder.build(), SkuInfo.class, new SearchResultMapper() {
            @Override
            public <T> AggregatedPage<T> mapResults(SearchResponse response, Class<T> clazz, Pageable pageable) {
                // 1.创建一个当前页的记录集合对象
                List<T> content = new ArrayList<>();

                if (response.getHits() == null || response.getHits().getTotalHits() <= 0) {
                    return new AggregatedPageImpl<T>(content);
                }

                // 搜索到的结果集
                for (SearchHit searchHit : response.getHits()) {
                    String sourceAsString = searchHit.getSourceAsString();// 每一个行的数据 json的 数据
                    SkuInfo skuInfo = JSON.parseObject(sourceAsString, SkuInfo.class);
                    Map<String, HighlightField> highlightFields = searchHit.getHighlightFields();// key :高亮的字段名  value 就是该字段的高亮的数据集合
                    HighlightField highlightField = highlightFields.get("name");
                    // 有高亮的数据
                    if (highlightField != null) {
                        StringBuilder buffer = new StringBuilder();// 有高亮的数据
                        // 取高亮的数据
                        for (Text text : highlightField.getFragments()) {
                            String string = text.string();// 高亮的数据  华为 胀奸  5寸  联通2G  白  <em style='color=red>'显示</em>  32G  16G  300万像素
                            buffer.append(string);
                        }
                        skuInfo.setName(buffer.toString());// 有高亮的数据
                    }
                    content.add((T) skuInfo);
                }

                // 2.创建分页的对象 已有
                // 3.获取总个记录数
                long totalHits = response.getHits().getTotalHits();
                // 4.获取所有聚合函数的结果
                Aggregations aggregations = response.getAggregations();
                // 5.深度分页的ID
                String scrollId = response.getScrollId();

                return new AggregatedPageImpl<T>(content, pageable, totalHits, aggregations, scrollId);
            }
        });

        Map<String, Object> result = new HashMap<>();
        result.put("rows", page.getContent());
        result.put("total", page.getTotalElements());
        result.put("totalPages", page.getTotalPages());

        // 分组查询分类集合
        // skuCategory为别名
        if (map == null || StringUtils.isEmpty(map.get("category"))) {
            result.put("categoryList", groupResult(page, "skuCategory"));
        }
        if (map == null || StringUtils.isEmpty(map.get("brand"))) {
            result.put("brandList", groupResult(page, "skuBrand"));
        }

        Map<String, TreeSet<String>> treeMap = group(groupResult(page, "skuSpec"));
        result.put("specList", treeMap);

        return result;
    }

    private Map<String, TreeSet<String>> group(List<String> specList) {
        Map<String, TreeSet<String>> treeMap = new TreeMap<>();
        specList.forEach(s -> {
            Map<String, String> stringStringMap = JSON.parseObject(s, new TypeReference<Map<String, String>>() {
            });
            stringStringMap.forEach((key, value) -> treeMap.compute(key, (k, v) -> {
                if (v == null) {
                    return new TreeSet<>();
                }
                v.add(value);
                return v;
            }));
        });
        return treeMap;
    }

    private void group(String name, String field, NativeSearchQueryBuilder builder) {
        // 分组查询
        // 每次默认显示10条，10000强制指定
        builder.addAggregation(AggregationBuilders.terms(name).field(field).size(10000));
    }

    private List<String> groupResult(AggregatedPage<SkuInfo> aggregatedPage, String name) {
        StringTerms stringTerms = (StringTerms) aggregatedPage.getAggregation(name);
        if (stringTerms == null) {
            return Collections.emptyList();
        }
        List<StringTerms.Bucket> buckets = stringTerms.getBuckets();
        return buckets.stream().map(StringTerms.Bucket::getKeyAsString).collect(Collectors.toList());
    }

}
