package com.changgou.search.controller;

import com.changgou.common.entity.StatusCode;
import com.changgou.common.response.Result;
import com.changgou.search.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 用于接收页面传递的请求 来测试 导入数据
 * 实现搜索的功能
 *
 * @version 1.0
 * @since 1.0
 */
@RestController
@CrossOrigin
@RequestMapping("/search")
public class SearchController {
    @Autowired
    private SearchService searchService;

    @GetMapping("/import")
    public Result<String> importEs() {

        searchService.importEs();
        return new Result<>(true, StatusCode.OK, "导入成功");
    }

    @GetMapping()
    public Result<Map<String, Object>> search(@RequestParam(required = false) Map<String, String> map) {

        Map<String, Object> result = searchService.search(map);
        return new Result<>(true, StatusCode.OK, "success!", result);
    }

}
