package com.changgou.page.service;

/**
 * 描述
 *
 * @version 1.0
 * @since 1.0
 */
public interface PageService {
    // 生成静态页
    void createPageHtml(Long id);

}
