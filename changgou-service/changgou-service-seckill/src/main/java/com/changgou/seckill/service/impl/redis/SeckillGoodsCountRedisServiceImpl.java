package com.changgou.seckill.service.impl.redis;

import com.changgou.seckill.constants.SystemConstants;
import com.changgou.seckill.service.SeckillGoodsCountRedisService;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author tcwgq
 * @since 2022/6/18 9:52
 */
@Service
public class SeckillGoodsCountRedisServiceImpl implements SeckillGoodsCountRedisService {
    @Resource(name = "goodsCountRedisTemplate")
    private RedisTemplate<String, Integer> redisTemplate;

    @Override
    public void add(Long goodId, Integer num) {
        redisTemplate.boundValueOps(SystemConstants.SECK_KILL_GOODS_COUNT_KEY + goodId).set(num);
    }

    @Override
    public Integer get(Long goodId) {
        BoundValueOperations<String, Integer> valueOperations = redisTemplate.boundValueOps(SystemConstants.SECK_KILL_GOODS_COUNT_KEY + goodId);
        return valueOperations.get();
    }

    @Override
    public void expireAt(Long goodId, Date date) {
        redisTemplate.expireAt(SystemConstants.SECK_KILL_GOODS_COUNT_KEY + goodId, date);
    }

    @Override
    public Long increase(Long goodId) {
        BoundValueOperations<String, Integer> valueOperations = redisTemplate.boundValueOps(SystemConstants.SECK_KILL_GOODS_COUNT_KEY + goodId);
        return valueOperations.increment();
    }

    @Override
    public Long decrease(Long goodId) {
        BoundValueOperations<String, Integer> valueOperations = redisTemplate.boundValueOps(SystemConstants.SECK_KILL_GOODS_COUNT_KEY + goodId);
        return valueOperations.decrement();
    }

    @Override
    public Boolean deleteCount(Long goodId) {
        return redisTemplate.delete(SystemConstants.SECK_KILL_GOODS_COUNT_KEY + goodId);
    }

    @Override
    public void putList(Long goodId, Integer... nums) {
        BoundListOperations<String, Integer> boundListOperations = redisTemplate.boundListOps(SystemConstants.SECK_KILL_GOODS_LIST_COUNT_KEY + goodId);
        boundListOperations.leftPushAll(nums);
    }

    @Override
    public Integer getList(Long goodId) {
        BoundListOperations<String, Integer> boundListOperations = redisTemplate.boundListOps(SystemConstants.SECK_KILL_GOODS_LIST_COUNT_KEY + goodId);
        return boundListOperations.rightPop();
    }

}
