package com.changgou.seckill.service;

import com.changgou.seckill.bean.SeckillGoods;

import java.util.List;

/**
 * SeckillGoodsService
 *
 * @author tcwgq
 * @since 2022/06/17 08:33
 */
public interface SeckillGoodsRedisService {
    List<SeckillGoods> list(String time);

    SeckillGoods get(String time, Long id);

    void put(String time, SeckillGoods seckillGoods);

    void delete(String time, Long id);

    void clearGoods(String time, Long id);

}
