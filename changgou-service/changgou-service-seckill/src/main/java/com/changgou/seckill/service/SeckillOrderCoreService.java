package com.changgou.seckill.service;

/**
 * SeckillOrderService
 *
 * @author tcwgq
 * @since 2022/06/17 08:33
 */
public interface SeckillOrderCoreService {
    boolean create(Long id, String time, String username);

    /**
     * 异步下单，队列实现
     */
    void createAsync();

}
