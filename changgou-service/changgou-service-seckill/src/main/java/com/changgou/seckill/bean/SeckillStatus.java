package com.changgou.seckill.bean;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 秒杀订单排队队列元素
 *
 * @version 1.0
 * @since 1.0
 */
@Data
public class SeckillStatus implements Serializable {
    // 订单号
    private Long orderId;
    // 用户名
    private String username;
    // 秒杀状态  1:排队中，2:秒杀等待支付,3:支付超时，4:秒杀失败,5:支付完成
    private Integer status;
    // 秒杀的商品ID
    private Long goodsId;
    // 应付金额
    private BigDecimal money;
    // 时间段
    private String time;
    // 创建时间
    private Date createTime;

    public SeckillStatus() {

    }

    public SeckillStatus(String username, Integer status, Long goodsId, String time, Date createTime) {
        this.username = username;
        this.status = status;
        this.goodsId = goodsId;
        this.time = time;
        this.createTime = createTime;
    }

}
