package com.changgou.seckill.service;

import java.util.Date;

/**
 * SeckillGoodsService
 *
 * @author tcwgq
 * @since 2022/06/17 08:33
 */
public interface SeckillGoodsCountRedisService {
    void add(Long goodId, Integer num);

    Integer get(Long goodId);

    void expireAt(Long goodId, Date date);

    Long increase(Long goodId);

    Long decrease(Long goodId);

    Boolean deleteCount(Long goodId);

    void putList(Long goodId, Integer... nums);

    Integer getList(Long goodId);

}
