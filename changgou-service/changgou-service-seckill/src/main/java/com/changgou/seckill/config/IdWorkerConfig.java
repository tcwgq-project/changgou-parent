package com.changgou.seckill.config;

import com.changgou.common.utils.IdWorker;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tcwgq
 * @since 2022/6/15 10:14
 */
@Configuration
public class IdWorkerConfig {
    @Bean
    public IdWorker idWorker() {
        return new IdWorker(0, 1);
    }

}
