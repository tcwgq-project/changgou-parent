package com.changgou.seckill.listener;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Slf4j
@Component
public class CustomMessageSender implements RabbitTemplate.ConfirmCallback {
    private static final String MESSAGE_CONFIRM_ = "message_confirm_";
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private StringRedisTemplate redisTemplate;

    public CustomMessageSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
        rabbitTemplate.setConfirmCallback(this);
    }

    @Override
    public void confirm(@Nullable CorrelationData correlationData, boolean ack, String cause) {
        if (ack) {
            String id = getId(correlationData);
            if (StringUtils.isNoneBlank(id)) {
                // 返回成功通知
                // 删除redis中的相关数据
                redisTemplate.delete(id);
                redisTemplate.delete(MESSAGE_CONFIRM_ + getId(correlationData));
            }
        } else {
            String id = getId(correlationData);
            if (StringUtils.isNoneBlank(id)) {
                // 返回失败通知
                BoundHashOperations<String, String, String> hashOperations = redisTemplate.boundHashOps(MESSAGE_CONFIRM_ + getId(correlationData));
                Map<String, String> map = hashOperations.entries();
                String exchange = map.get("exchange");
                String routingKey = map.get("routingKey");
                String sendMessage = map.get("sendMessage");
                // 重新发送
                rabbitTemplate.convertAndSend(exchange, routingKey, JSON.toJSONString(sendMessage));
            }
        }
    }

    // 自定义发送方法
    public void sendMessage(String exchange, String routingKey, String message) {
        // 设置消息唯一标识并存入缓存
        CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString());
        redisTemplate.opsForValue().set(getId(correlationData), message);
        // 本次发送到相关元信息存入缓存
        Map<String, String> map = new HashMap<>();
        map.put("exchange", exchange);
        map.put("routingKey", routingKey);
        map.put("sendMessage", message);
        redisTemplate.opsForHash().putAll(MESSAGE_CONFIRM_ + getId(correlationData), map);
        // 携带唯一标识发送消息
        rabbitTemplate.convertAndSend(exchange, routingKey, message, correlationData);
    }

    private String getId(CorrelationData correlationData) {
        if (correlationData == null) {
            return null;
        }
        return correlationData.getId();
    }

}