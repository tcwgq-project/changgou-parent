package com.changgou.seckill.dao;

import com.changgou.seckill.bean.SeckillOrder;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

/**
 * SeckillOrderDao
 *
 * @author tcwgq
 * @since 2022/06/17 08:33
 */
public interface SeckillOrderMapper extends Mapper<SeckillOrder> {
    /**
     * 查询秒杀订单信息
     *
     * @param username
     * @param id
     */
    @Select("select * from tb_seckill_order where user_id = #{username} and seckill_id = #{id}")
    SeckillOrder selectByUsername(@Param("username") String username, @Param("id") Long id);

}
