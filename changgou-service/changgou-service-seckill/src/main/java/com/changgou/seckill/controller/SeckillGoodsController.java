package com.changgou.seckill.controller;

import com.changgou.common.entity.StatusCode;
import com.changgou.common.response.Result;
import com.changgou.seckill.bean.SeckillGoods;
import com.changgou.seckill.service.SeckillGoodsService;
import com.changgou.seckill.util.DateUtil;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * SeckillGoodsController
 *
 * @author tcwgq
 * @since 2022/06/17 08:33
 */

@RestController
@RequestMapping("/seckillGoods")
@CrossOrigin
public class SeckillGoodsController {
    @Resource
    private SeckillGoodsService seckillGoodsService;

    /**
     * 新增SeckillGoods数据
     *
     * @param seckillGoods
     * @return
     */
    @PostMapping
    public Result<String> add(@RequestBody SeckillGoods seckillGoods) {
        // 调用SeckillGoodsService实现添加SeckillGoods
        seckillGoodsService.add(seckillGoods);
        return new Result<>(true, StatusCode.OK, "添加成功");
    }

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    public Result<String> delete(@PathVariable Long id) {
        // 调用SeckillGoodsService实现根据主键删除
        seckillGoodsService.delete(id);
        return new Result<>(true, StatusCode.OK, "删除成功");
    }

    /**
     * 修改SeckillGoods数据
     *
     * @param seckillGoods
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    public Result<String> update(@RequestBody SeckillGoods seckillGoods, @PathVariable Long id) {
        // 设置主键值
        seckillGoods.setId(id);
        // 调用SeckillGoodsService实现修改SeckillGoods
        seckillGoodsService.update(seckillGoods);
        return new Result<>(true, StatusCode.OK, "修改成功");
    }

    /**
     * 根据ID查询SeckillGoods数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<SeckillGoods> findById(@PathVariable Long id) {
        // 调用SeckillGoodsService实现根据主键查询SeckillGoods
        SeckillGoods seckillGoods = seckillGoodsService.findById(id);
        return new Result<>(true, StatusCode.OK, "查询成功", seckillGoods);
    }

    /**
     * 查询SeckillGoods全部数据
     *
     * @return
     */
    @GetMapping
    public Result<List<SeckillGoods>> findAll() {
        // 调用SeckillGoodsService实现查询所有SeckillGoods
        List<SeckillGoods> list = seckillGoodsService.findAll();
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    /**
     * 多条件搜索品牌数据
     *
     * @param seckillGoods
     * @return
     */
    @PostMapping(value = "/search")
    public Result<List<SeckillGoods>> findList(@RequestBody(required = false) SeckillGoods seckillGoods) {
        // 调用SeckillGoodsService实现条件查询SeckillGoods
        List<SeckillGoods> list = seckillGoodsService.findList(seckillGoods);
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    /**
     * 获取当前的时间基准的5个时间段
     *
     * @return
     */
    @GetMapping("/menus")
    public Result<List<String>> menus() {
        List<Date> dateMenus = DateUtil.getDateMenus("2019-05-23 16:10:00");
        List<String> list = dateMenus.stream().map(date -> DateUtil.data2str(date, DateUtil.PATTERN_YYYYMMDDHH)).collect(Collectors.toList());

        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    @GetMapping(value = "/list")
    public Result<List<SeckillGoods>> list(@RequestParam("time") String time) {
        // 调用SeckillGoodsService实现条件查询SeckillGoods
        List<SeckillGoods> list = seckillGoodsService.list(time);
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    @PostMapping(value = "/one")
    public Result<SeckillGoods> one(@RequestParam("time") String time, @RequestParam("id") Long id) {
        // 调用SeckillGoodsService实现条件查询SeckillGoods
        SeckillGoods one = seckillGoodsService.one(time, id);
        return new Result<>(true, StatusCode.OK, "查询成功", one);
    }

    /**
     * SeckillGoods分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<SeckillGoods>> findPage(@PathVariable int page, @PathVariable int size) {
        // 调用SeckillGoodsService实现分页查询SeckillGoods
        PageInfo<SeckillGoods> pageInfo = seckillGoodsService.findPage(page, size);
        return new Result<>(true, StatusCode.OK, "查询成功", pageInfo);
    }

    /**
     * SeckillGoods分页条件搜索实现
     *
     * @param seckillGoods
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<SeckillGoods>> findPage(@RequestBody(required = false) SeckillGoods seckillGoods, @PathVariable int page, @PathVariable int size) {
        // 调用SeckillGoodsService实现分页条件查询SeckillGoods
        PageInfo<SeckillGoods> pageInfo = seckillGoodsService.findPage(seckillGoods, page, size);
        return new Result<>(true, StatusCode.OK, "查询成功", pageInfo);
    }

}
