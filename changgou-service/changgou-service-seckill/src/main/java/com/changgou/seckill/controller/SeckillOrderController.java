package com.changgou.seckill.controller;

import com.changgou.common.entity.StatusCode;
import com.changgou.common.response.Result;
import com.changgou.common_spring.utils.TokenDecoder;
import com.changgou.seckill.bean.SeckillOrder;
import com.changgou.seckill.bean.SeckillStatus;
import com.changgou.seckill.service.SeckillOrderService;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * SeckillOrderController
 *
 * @author tcwgq
 * @since 2022/06/17 08:33
 */

@RestController
@RequestMapping("/seckillOrder")
@CrossOrigin
public class SeckillOrderController {
    @Resource
    private SeckillOrderService seckillOrderService;

    /**
     * 接口加密
     * 生成随机数存入redis，10秒有效期
     */
    @GetMapping("/random")
    public Result<String> random() {
        String username = TokenDecoder.getUsername();
        String randomCode = seckillOrderService.getRandomCode(username);
        return new Result<>(true, StatusCode.OK, "OK", randomCode);
    }

    /**
     * 秒杀下单
     *
     * @param time 当前时间段
     * @param id   秒杀商品id
     */
    @PostMapping("/add")
    public Result<String> add(@RequestParam("time") String time,
                              @RequestParam("id") Long id,
                              @RequestParam("random") String random) {
        // 获取当前登陆人
        String username = TokenDecoder.getUsername();
        boolean result = seckillOrderService.add(id, time, username, random);
        if (result) {
            return new Result<>(true, StatusCode.OK, "下单成功");
        } else {
            return new Result<>(false, StatusCode.ERROR, "下单失败");
        }
    }

    /**
     * 下单
     */
    @PostMapping("/create")
    public Result<Boolean> create(@RequestParam("time") String time, @RequestParam("id") Long id) {
        // 1.获取当前登录的用户的名称
        String username = TokenDecoder.getUsername();
        // 2.调用service的方法创建订单
        boolean result = seckillOrderService.create(id, time, username);

        return new Result<>(true, StatusCode.OK, "正在排队", result);
    }

    @GetMapping("/query/status")
    public Result<SeckillStatus> status() {
        String username = TokenDecoder.getUsername();
        SeckillStatus seckillStatus = seckillOrderService.status(username);
        if (seckillStatus != null) {
            return new Result<>(true, StatusCode.OK, "查询成功!", seckillStatus);
        }

        return new Result<>(true, StatusCode.NOT_FOUND_ERROR, "抢购信息没有");
    }

    /**
     * 新增SeckillOrder数据
     *
     * @param seckillOrder
     * @return
     */
    @PostMapping
    public Result<String> add(@RequestBody SeckillOrder seckillOrder) {
        // 调用SeckillOrderService实现添加SeckillOrder
        seckillOrderService.add(seckillOrder);
        return new Result<>(true, StatusCode.OK, "添加成功");
    }

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    public Result<String> delete(@PathVariable Long id) {
        // 调用SeckillOrderService实现根据主键删除
        seckillOrderService.delete(id);
        return new Result<>(true, StatusCode.OK, "删除成功");
    }

    /**
     * 修改SeckillOrder数据
     *
     * @param seckillOrder
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    public Result<String> update(@RequestBody SeckillOrder seckillOrder, @PathVariable Long id) {
        // 设置主键值
        seckillOrder.setId(id);
        // 调用SeckillOrderService实现修改SeckillOrder
        seckillOrderService.update(seckillOrder);
        return new Result<>(true, StatusCode.OK, "修改成功");
    }

    /**
     * 根据ID查询SeckillOrder数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<SeckillOrder> findById(@PathVariable Long id) {
        // 调用SeckillOrderService实现根据主键查询SeckillOrder
        SeckillOrder seckillOrder = seckillOrderService.findById(id);
        return new Result<>(true, StatusCode.OK, "查询成功", seckillOrder);
    }

    /**
     * 查询SeckillOrder全部数据
     *
     * @return
     */
    @GetMapping
    public Result<List<SeckillOrder>> findAll() {
        // 调用SeckillOrderService实现查询所有SeckillOrder
        List<SeckillOrder> list = seckillOrderService.findAll();
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    /**
     * 多条件搜索品牌数据
     *
     * @param seckillOrder
     * @return
     */
    @PostMapping(value = "/search")
    public Result<List<SeckillOrder>> findList(@RequestBody(required = false) SeckillOrder seckillOrder) {
        // 调用SeckillOrderService实现条件查询SeckillOrder
        List<SeckillOrder> list = seckillOrderService.findList(seckillOrder);
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    /**
     * SeckillOrder分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<SeckillOrder>> findPage(@PathVariable int page, @PathVariable int size) {
        // 调用SeckillOrderService实现分页查询SeckillOrder
        PageInfo<SeckillOrder> pageInfo = seckillOrderService.findPage(page, size);
        return new Result<>(true, StatusCode.OK, "查询成功", pageInfo);
    }

    /**
     * SeckillOrder分页条件搜索实现
     *
     * @param seckillOrder
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<SeckillOrder>> findPage(@RequestBody(required = false) SeckillOrder seckillOrder, @PathVariable int page, @PathVariable int size) {
        // 调用SeckillOrderService实现分页条件查询SeckillOrder
        PageInfo<SeckillOrder> pageInfo = seckillOrderService.findPage(seckillOrder, page, size);
        return new Result<>(true, StatusCode.OK, "查询成功", pageInfo);
    }

}
