package com.changgou.seckill.service;

import com.changgou.seckill.bean.SeckillOrder;

/**
 * SeckillOrderService
 *
 * @author tcwgq
 * @since 2022/06/17 08:33
 */
public interface SeckillOrderRedisService {
    void put(String username, SeckillOrder seckillOrder);

    SeckillOrder get(String username);

    void delete(String username);

}
