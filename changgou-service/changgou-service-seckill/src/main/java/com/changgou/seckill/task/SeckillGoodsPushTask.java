package com.changgou.seckill.task;

import com.changgou.seckill.bean.SeckillGoods;
import com.changgou.seckill.constants.SystemConstants;
import com.changgou.seckill.dao.SeckillGoodsMapper;
import com.changgou.seckill.service.SeckillGoodsCountRedisService;
import com.changgou.seckill.util.DateUtil;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * TODO 多实例情况下，存在并发问题，需要加锁
 * 定时任务 spring task(多线程)
 * 1.开始spring task
 * 2.在执行的方法上修饰一个注解  注解中指定何时执行即可
 *
 * @version 1.0
 * @since 1.0
 */
@Component
public class SeckillGoodsPushTask implements InitializingBean {
    @Resource
    private SeckillGoodsMapper seckillGoodsMapper;

    @Resource(name = "goodsRedisTemplate")
    private RedisTemplate<String, SeckillGoods> redisTemplate;

    @Resource
    private SeckillGoodsCountRedisService seckillGoodsCountRedisService;

    @Override
    public void afterPropertiesSet() {

    }

    // 反复被执行的方法 隔5秒钟执行一次
    @Scheduled(cron = "0/5 * * * * ?")
    public void loadGoodsPushRedis() {
        // 1.获取当前的时间对应的5个时间段
        List<Date> dateMenus = DateUtil.getDateMenus("2019-05-23 16:10:00");
        // 2.循环遍历5个时间段 获取到时间的日期
        for (Date startTime : dateMenus) {
            // 2019090516
            String period = DateUtil.data2str(startTime, DateUtil.PATTERN_YYYYMMDDHH);
            /*
             * 3.将循环到的时间段 作为条件 从数据库中执行查询 得出数据集
             * select * from tb_seckill_goods where stock_count > 0 and `status`='1' and start_time > 开始时间段 and end_time < 开始时间段 + 2hour and id not in (redis中已有的id)
             */
            Example example = new Example(SeckillGoods.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("status", "1");
            criteria.andGreaterThan("stockCount", 0);
            criteria.andGreaterThanOrEqualTo("startTime", startTime);
            criteria.andLessThan("endTime", DateUtil.addDateHour(startTime, 2));

            // 排除掉redis已有的商品

            BoundHashOperations<String, Long, SeckillGoods> boundHashOperations = redisTemplate.boundHashOps(getNamespace(period));
            Set<Long> keys = boundHashOperations.keys();
            if (keys != null && keys.size() > 0) {
                criteria.andNotIn("id", keys);
            }

            List<SeckillGoods> seckillGoods = seckillGoodsMapper.selectByExample(example);
            /*
             * 4.将数据集存储到redis中(key field value的数据格式 )
             *  key(时间段:2019090516) field (id:1) value(商品的数据pojo)
             */
            for (SeckillGoods seckillGood : seckillGoods) {
                redisTemplate.boundHashOps(getNamespace(period)).put(seckillGood.getId(), seckillGood);
                // 设置有效期
                redisTemplate.expireAt(getNamespace(period), DateUtil.addDateHour(new Date(), 2));
                // 设置物品数量
                seckillGoodsCountRedisService.add(seckillGood.getId(), seckillGood.getStockCount());
                // seckillGoodsCountRedisService.expireAt(seckillGood.getId(), seckillGood.getEndTime());
            }
        }
    }

    private String getNamespace(String period) {
        return SystemConstants.SEC_KILL_GOODS_PREFIX + period;
    }

}
