package com.changgou.seckill.config;

import com.changgou.seckill.bean.SeckillGoods;
import com.changgou.seckill.bean.SeckillOrder;
import com.changgou.seckill.bean.SeckillStatus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * 注意：每个类中单独设置RedisTemplate序列化器的时候，会存在覆盖问题，所以统一拿到这里声明
 *
 * @author tcwgq
 * @since 2022/6/17 19:28
 */
@Configuration
public class RedisTemplateConfig {
    @Bean("goodsRedisTemplate")
    public RedisTemplate<String, SeckillGoods> goodsRedisTemplate(RedisConnectionFactory connectionFactory) {
        RedisTemplate<String, SeckillGoods> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(connectionFactory);
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<>(SeckillGoods.class));
        redisTemplate.setHashKeySerializer(new Jackson2JsonRedisSerializer<>(Long.class));
        redisTemplate.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(SeckillGoods.class));
        return redisTemplate;
    }

    @Bean("goodsCountRedisTemplate")
    public RedisTemplate<String, Integer> goodsCountRedisTemplate(RedisConnectionFactory connectionFactory) {
        RedisTemplate<String, Integer> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(connectionFactory);
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<>(Integer.class));
        redisTemplate.setHashKeySerializer(new Jackson2JsonRedisSerializer<>(Integer.class));
        redisTemplate.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(Integer.class));
        return redisTemplate;
    }

    @Bean("orderRedisTemplate")
    public RedisTemplate<String, SeckillOrder> orderRedisTemplate(RedisConnectionFactory connectionFactory) {
        RedisTemplate<String, SeckillOrder> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(connectionFactory);
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<>(SeckillOrder.class));
        redisTemplate.setHashKeySerializer(new Jackson2JsonRedisSerializer<>(Long.class));
        redisTemplate.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(SeckillOrder.class));
        return redisTemplate;
    }

    @Bean("statusRedisTemplate")
    public RedisTemplate<String, SeckillStatus> statusRedisTemplate(RedisConnectionFactory connectionFactory) {
        RedisTemplate<String, SeckillStatus> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(connectionFactory);
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<>(SeckillStatus.class));
        redisTemplate.setHashKeySerializer(new Jackson2JsonRedisSerializer<>(String.class));
        redisTemplate.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(SeckillStatus.class));
        return redisTemplate;
    }
}
