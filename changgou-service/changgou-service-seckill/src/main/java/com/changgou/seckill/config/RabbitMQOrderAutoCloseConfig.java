package com.changgou.seckill.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class RabbitMQOrderAutoCloseConfig {
    // 普通队列名称
    public static final String ORDER_AUTO_EXPIRE_QUEUE = "seckill_order_auto_expire_queue";
    // 普通交换机名称
    public static final String ORDER_AUTO_EXPIRE_EXCHANGE = "seckill_order_auto_expire_exchange";
    // 死信队列名称
    public static final String ORDER_AUTO_EXPIRE_DEAD_QUEUE = "seckill_order_auto_expire_dead_queue";
    // 死信交换机名称
    public static final String ORDER_AUTO_EXPIRE_DEAD_QUEUE_EXCHANGE = "seckill_order_auto_expire_dead_queue_exchange";

    // 声明普通队列,TTL为30min
    @Bean(ORDER_AUTO_EXPIRE_QUEUE)
    public Queue queue() {
        Map<String, Object> arguments = new HashMap<>();
        // 设置死信交换机
        arguments.put("x-dead-letter-exchange", ORDER_AUTO_EXPIRE_DEAD_QUEUE_EXCHANGE);
        // 设置死信RoutingKey
        arguments.put("x-dead-letter-routing-key", "YD");
        // 设置TTL，单位毫秒
        arguments.put("x-message-ttl", 10 * 60 * 1000);

        return QueueBuilder.durable(ORDER_AUTO_EXPIRE_QUEUE).withArguments(arguments).build();
    }

    // 声明普通交换机
    @Bean(ORDER_AUTO_EXPIRE_EXCHANGE)
    public DirectExchange exchange() {
        return new DirectExchange(ORDER_AUTO_EXPIRE_EXCHANGE);
    }

    // 声明死信队列
    @Bean(ORDER_AUTO_EXPIRE_DEAD_QUEUE)
    public Queue queue1() {
        return QueueBuilder.durable(ORDER_AUTO_EXPIRE_DEAD_QUEUE).build();
    }

    // 声明死信交换机
    @Bean(ORDER_AUTO_EXPIRE_DEAD_QUEUE_EXCHANGE)
    public DirectExchange exchange1() {
        return new DirectExchange(ORDER_AUTO_EXPIRE_DEAD_QUEUE_EXCHANGE);
    }

    // 绑定对应的交换机和队列
    @Bean(ORDER_AUTO_EXPIRE_QUEUE + "_" + ORDER_AUTO_EXPIRE_EXCHANGE)
    public Binding queueBindingExchange(@Qualifier(ORDER_AUTO_EXPIRE_QUEUE) Queue queue,
                                        @Qualifier(ORDER_AUTO_EXPIRE_EXCHANGE) DirectExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("XA");
    }

    @Bean(ORDER_AUTO_EXPIRE_DEAD_QUEUE + "_" + ORDER_AUTO_EXPIRE_DEAD_QUEUE_EXCHANGE)
    public Binding queue1BindingExchange1(@Qualifier(ORDER_AUTO_EXPIRE_DEAD_QUEUE) Queue queue,
                                          @Qualifier(ORDER_AUTO_EXPIRE_DEAD_QUEUE_EXCHANGE) DirectExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("YD");
    }

}