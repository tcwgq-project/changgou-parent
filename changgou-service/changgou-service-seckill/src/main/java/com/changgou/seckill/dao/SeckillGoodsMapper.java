package com.changgou.seckill.dao;

import com.changgou.seckill.bean.SeckillGoods;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.Mapper;

/**
 * SeckillGoodsDao
 *
 * @author tcwgq
 * @since 2022/06/17 08:33
 */
public interface SeckillGoodsMapper extends Mapper<SeckillGoods> {

    @Update(value = "update tb_seckill_goods set stock_count = stock_count - #{num}, num = num + #{num} where id = #{skuId} and stock_count >= #{num}")
    int decrease(@Param("skuId") Long skuId, @Param("num") Integer num);

    @Update(value = "update tb_seckill_goods set stock_count = stock_count + #{num}, num = num - #{num} where id = #{skuId} and num > #{num}")
    int increase(@Param("skuId") Long skuId, @Param("num") Integer num);

}
