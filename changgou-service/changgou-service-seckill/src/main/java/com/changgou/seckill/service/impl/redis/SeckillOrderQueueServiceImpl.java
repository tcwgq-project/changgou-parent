package com.changgou.seckill.service.impl.redis;

import com.changgou.seckill.bean.SeckillStatus;
import com.changgou.seckill.constants.SystemConstants;
import com.changgou.seckill.service.SeckillOrderQueueService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * SeckillOrderServiceImpl
 *
 * @author tcwgq
 * @since 2022/06/17 08:33
 */
@Service
public class SeckillOrderQueueServiceImpl implements SeckillOrderQueueService, InitializingBean {
    @Resource(name = "statusRedisTemplate")
    private RedisTemplate<String, SeckillStatus> redisTemplate;

    @Override
    public void afterPropertiesSet() {

    }

    @Override
    public Long increment(String username) {
        return redisTemplate.boundValueOps(SystemConstants.SEC_KILL_QUEUE_REPEAT_KEY + username).increment();
    }

    @Override
    public Boolean deleteCount(String username) {
        return redisTemplate.delete(SystemConstants.SEC_KILL_QUEUE_REPEAT_KEY + username);
    }

    @Override
    public void leftPush(SeckillStatus seckillStatus) {
        // 进入排队中
        redisTemplate.boundListOps(SystemConstants.SEC_KILL_USER_QUEUE_KEY + "list").leftPush(seckillStatus);
    }

    @Override
    public SeckillStatus rightPop() {
        return redisTemplate.boundListOps(SystemConstants.SEC_KILL_USER_QUEUE_KEY + "list").rightPop();
    }

    @Override
    public void put(String username, SeckillStatus seckillStatus) {
        // 进入排队标识
        redisTemplate.boundHashOps(SystemConstants.SEC_KILL_USER_STATUS_KEY + "list").put(username, seckillStatus);
    }

    @Override
    public void remove(String username) {
        // 进入排队标识
        redisTemplate.boundHashOps(SystemConstants.SEC_KILL_USER_STATUS_KEY + "list").delete(username);
    }

    @Override
    public SeckillStatus status(String username) {
        return (SeckillStatus) redisTemplate.boundHashOps(SystemConstants.SEC_KILL_USER_STATUS_KEY + "list").get(username);
    }

    @Override
    public void clearUserQueue(String username) {
        // 清除重复排队计数
        deleteCount(username);
        // 清除排队信息
        remove(username);
    }

}
