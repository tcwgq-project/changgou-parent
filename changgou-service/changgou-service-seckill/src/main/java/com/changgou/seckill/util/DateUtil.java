package com.changgou.seckill.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateUtil {
    // 时间格式
    public static final String PATTERN_YYYYMMDDHH = "yyyyMMddHH";
    public static final String PATTERN_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    /***
     * 从yyyy-MM-dd HH:mm格式转成yyyyMMddHH格式
     */
    public static Date parse(String dateStr, String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        try {
            return simpleDateFormat.parse(dateStr);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    /***
     * 时间转成yyyyMMddHH
     */
    public static String data2str(Date date, String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(date);
    }

    /***
     * 获取指定日期的凌晨
     */
    public static Date toDayStartHour(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /***
     * 时间递增N小时
     */
    public static Date addDateHour(Date date, int hour) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, hour);// 24小时制
        date = calendar.getTime();
        return date;
    }

    /***
     * 时间增加N分钟
     */
    public static Date addDateMinutes(Date date, int minutes) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, minutes);// 24小时制
        date = calendar.getTime();
        return date;
    }

    /***
     * 获取时间菜单
     */
    public static List<Date> getDateMenus(String nowString) {
        // 定义一个List<Date>集合，存储所有时间段
        Date now = parse(nowString, PATTERN_YYYY_MM_DD_HH_MM_SS);
        List<Date> dates = getDates(12, now);
        // 判断当前时间属于哪个时间范围
        for (Date date : dates) {
            // 开始时间<=当前时间<开始时间+2小时
            if (date.getTime() <= now.getTime() && now.getTime() < addDateHour(date, 2).getTime()) {
                now = date;
                break;
            }
        }

        // 当前需要显示的时间菜单
        List<Date> dateMenus = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            dateMenus.add(addDateHour(now, i * 2));
        }
        return dateMenus;
    }

    /***
     * 指定时间往后N个时间间隔
     */
    public static List<Date> getDates(int hours, Date now) {
        List<Date> dates = new ArrayList<>();
        // 循环12次
        Date date = toDayStartHour(now); // 凌晨
        for (int i = 0; i < hours; i++) {
            // 每次递增2小时,将每次递增的时间存入到List<Date>集合中
            dates.add(addDateHour(date, i * 2));
        }
        return dates;
    }

}
