package com.changgou.seckill.mq;

import com.changgou.seckill.config.RabbitMQOrderAutoCloseConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author tcwgq
 * @since 2022/6/18 18:12
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class DelayMqTest {
    @Resource
    private RabbitTemplate rabbitTemplate;

    @Test
    public void sendSeckillOrderDelay() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String time = formatter.format(LocalDateTime.now());
        rabbitTemplate.convertAndSend(
                RabbitMQOrderAutoCloseConfig.ORDER_AUTO_EXPIRE_EXCHANGE,
                "XA",
                time + "，消息来自TTL为10s的队列QA：" + "Hello, world!");
    }

}
