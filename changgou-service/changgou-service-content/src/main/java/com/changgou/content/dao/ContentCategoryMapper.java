package com.changgou.content.dao;

import com.changgou.content.pojo.ContentCategory;
import tk.mybatis.mapper.common.Mapper;

/**
 * ContentCategoryDao
 *
 * @author tcwgq
 * @since 2022/05/29 20:00
 */
public interface ContentCategoryMapper extends Mapper<ContentCategory> {

}
