package com.changgou.content.service;

import com.changgou.content.pojo.ContentCategory;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * ContentCategoryService
 *
 * @author tcwgq
 * @since 2022/05/29 20:00
 */
public interface ContentCategoryService {
    /**
     * 新增ContentCategory
     *
     * @param contentCategory
     */
    void add(ContentCategory contentCategory);

    /**
     * 删除ContentCategory
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 修改ContentCategory数据
     *
     * @param contentCategory
     */
    void update(ContentCategory contentCategory);

    /**
     * 根据ID查询ContentCategory
     *
     * @param id
     * @return
     */
    ContentCategory findById(Long id);

    /**
     * 查询所有ContentCategory
     *
     * @return
     */
    List<ContentCategory> findAll();

    /**
     * ContentCategory多条件搜索方法
     *
     * @param contentCategory
     * @return
     */
    List<ContentCategory> findList(ContentCategory contentCategory);

    /**
     * ContentCategory分页查询
     *
     * @param page
     * @param size
     * @return
     */
    PageInfo<ContentCategory> findPage(int page, int size);

    /**
     * ContentCategory多条件分页查询
     *
     * @param contentCategory
     * @param page
     * @param size
     * @return
     */
    PageInfo<ContentCategory> findPage(ContentCategory contentCategory, int page, int size);

}
