package com.changgou.goods.dao;

import com.changgou.goods.pojo.Para;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 通用mapper实现
 *
 * @author tcwgq
 * @since 2020/9/14 17:57
 */
public interface ParaMapper extends Mapper<Para> {
    @Select("select tp.* from tb_category tc, tb_para tp where tc.template_id = tp.template_id and tc.id = #{categoryId}")
    List<Para> findByCategoryId(@Param("categoryId") Long categoryId);

}
