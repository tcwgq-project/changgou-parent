package com.changgou.goods.service;

import com.changgou.goods.pojo.Para;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @author tcwgq
 * @since 2020/9/14 17:57
 */
public interface ParaService {
    int add(Para para);

    int delete(Long id);

    int updateById(Long id, Para para);

    int update(Para para);

    Para findById(Long id);

    List<Para> findByCategoryId(Long categoryId);

    List<Para> findList(Para para);

    List<Para> findAll();

    PageInfo<Para> page(Integer page, Integer size);

    PageInfo<Para> page(Integer page, Integer size, Para para);
}
