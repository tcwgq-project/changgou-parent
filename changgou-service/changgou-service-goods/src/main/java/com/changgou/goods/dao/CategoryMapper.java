package com.changgou.goods.dao;

import com.changgou.goods.pojo.Category;
import tk.mybatis.mapper.common.Mapper;

/**
 * 通用mapper实现
 *
 * @author tcwgq
 * @since 2020/9/14 17:57
 */
public interface CategoryMapper extends Mapper<Category> {

}
