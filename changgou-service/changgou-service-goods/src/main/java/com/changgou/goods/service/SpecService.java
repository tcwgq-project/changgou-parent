package com.changgou.goods.service;

import com.changgou.goods.pojo.Spec;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @author tcwgq
 * @since 2020/9/14 17:57
 */
public interface SpecService {
    int add(Spec spec);

    int delete(Long id);

    int updateById(Long id, Spec spec);

    int update(Spec spec);

    Spec findById(Long id);

    List<Spec> findByCategoryId(Long categoryId);

    List<Spec> findList(Spec spec);

    List<Spec> findAll();

    PageInfo<Spec> page(Integer page, Integer size);

    PageInfo<Spec> page(Integer page, Integer size, Spec spec);
}
