package com.changgou.goods.service;

import com.changgou.goods.pojo.Goods;
import com.changgou.goods.pojo.Spu;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @author tcwgq
 * @since 2022/5/28 10:18
 */
public interface SpuService {
    void add(Spu spu);

    void delete(Long id);

    void update(Spu spu);

    void save(Goods goods);

    void audit(Long id);

    void pull(Long id);

    void put(Long id);

    void putMany(Long[] ids);

    void logicDeleteSpu(Long id);

    void restoreSpu(Long id);

    Spu findById(Long id);

    Goods findGoodsById(Long spuId);

    List<Spu> findAll();

    List<Spu> findList(Spu spu);

    PageInfo<Spu> findPage(int page, int size);

    PageInfo<Spu> findPage(Spu spu, int page, int size);

}
