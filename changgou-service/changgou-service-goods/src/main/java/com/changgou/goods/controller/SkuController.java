package com.changgou.goods.controller;

import com.changgou.common.entity.StatusCode;
import com.changgou.common.response.Result;
import com.changgou.goods.pojo.Sku;
import com.changgou.goods.service.SkuService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author tcwgq
 * @since 2022/5/28 10:18
 */
@Api(value = "SkuController")
@RestController
@RequestMapping("/sku")
@CrossOrigin
public class SkuController {
    @Resource
    private SkuService skuService;

    @PostMapping
    public Result<String> add(@RequestBody Sku sku) {
        // 调用SkuService实现添加Sku
        skuService.add(sku);
        return new Result<>(true, StatusCode.OK, "添加成功");
    }

    @DeleteMapping(value = "/{id}")
    public Result<String> delete(@PathVariable Long id) {
        // 调用SkuService实现根据主键删除
        skuService.delete(id);
        return new Result<>(true, StatusCode.OK, "删除成功");
    }

    @PutMapping(value = "/{id}")
    public Result<String> update(@RequestBody Sku sku, @PathVariable Long id) {
        // 设置主键值
        sku.setId(id);
        // 调用SkuService实现修改Sku
        skuService.update(sku);
        return new Result<>(true, StatusCode.OK, "修改成功");
    }

    /**
     * 减库存
     *
     * @param map Map<SkuId, num>
     * @return
     */
    @PostMapping(value = "/decrease")
    public Result<String> decrease(@RequestBody Map<Long, Integer> map) {
        skuService.decrease(map);
        return new Result<>(true, StatusCode.OK, "减少库存成功");
    }

    /**
     * 加库存
     *
     * @param map Map<SkuId, num>
     * @return
     */
    @PostMapping(value = "/increase")
    public Result<String> increase(@RequestBody Map<Long, Integer> map) {
        skuService.increase(map);
        return new Result<>(true, StatusCode.OK, "增加库存成功");
    }

    @GetMapping("/{id}")
    public Result<Sku> findById(@PathVariable Long id) {
        // 调用SkuService实现根据主键查询Sku
        Sku sku = skuService.findById(id);
        return new Result<>(true, StatusCode.OK, "查询成功", sku);
    }

    @GetMapping
    public Result<List<Sku>> findAll() {
        // 调用SkuService实现查询所有Sku
        List<Sku> list = skuService.findAll();
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    @PostMapping(value = "/search")
    public Result<List<Sku>> findList(@RequestBody(required = false) Sku sku) {
        // 调用SkuService实现条件查询Sku
        List<Sku> list = skuService.findList(sku);
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    @GetMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<Sku>> findPage(@PathVariable int page, @PathVariable int size) {
        // 调用SkuService实现分页查询Sku
        PageInfo<Sku> pageInfo = skuService.findPage(page, size);
        return new Result<>(true, StatusCode.OK, "查询成功", pageInfo);
    }

    @PostMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<Sku>> findPage(@RequestBody(required = false) Sku sku,
                                          @PathVariable int page, @PathVariable int size) {
        // 调用SkuService实现分页条件查询Sku
        PageInfo<Sku> pageInfo = skuService.findPage(sku, page, size);
        return new Result<>(true, StatusCode.OK, "查询成功", pageInfo);
    }

}
