package com.changgou.goods.service;

import com.changgou.goods.pojo.Category;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @author tcwgq
 * @since 2022/5/27 17:57
 */
public interface CategoryService {
    int add(Category category);

    int delete(Long id);

    int updateById(Long id, Category category);

    int update(Category category);

    Category findById(Long id);

    List<Category> findByParentId(Long parentId);

    List<Category> findList(Category category);

    List<Category> findAll();

    PageInfo<Category> page(Integer page, Integer size);

    PageInfo<Category> page(Integer page, Integer size, Category category);

}
