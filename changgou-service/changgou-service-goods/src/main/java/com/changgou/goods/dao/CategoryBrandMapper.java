package com.changgou.goods.dao;

import com.changgou.goods.pojo.CategoryBrand;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author tcwgq
 * @since 2022/5/28 10:18
 */
public interface CategoryBrandMapper extends Mapper<CategoryBrand> {

}
