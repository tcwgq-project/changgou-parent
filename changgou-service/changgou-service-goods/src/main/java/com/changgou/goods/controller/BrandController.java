package com.changgou.goods.controller;

import com.changgou.common.response.Result;
import com.changgou.goods.pojo.Brand;
import com.changgou.goods.service.BrandService;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tcwgq
 * @since 2020/9/14 17:58
 */
@RestController
@RequestMapping("/brand")
@CrossOrigin
public class BrandController {
    @Resource
    private BrandService brandService;

    @PostMapping("/add")
    public Result<Integer> add(@RequestBody Brand brand) {
        return Result.success(brandService.add(brand));
    }

    @DeleteMapping("/{id}")
    public Result<Integer> delete(@PathVariable("id") Long id) {
        return Result.success(brandService.delete(id));
    }

    @PutMapping("/{id}")
    public Result<Integer> updateById(@PathVariable("id") Long id, @RequestBody Brand brand) {
        return Result.success(brandService.updateById(id, brand));
    }

    @PostMapping("/update")
    public Result<Integer> update(@RequestBody Brand brand) {
        return Result.success(brandService.update(brand));
    }

    @GetMapping("/{id}")
    public Result<Brand> findById(@PathVariable("id") Long id) {
        return Result.success(brandService.findById(id));
    }

    @GetMapping("/category/{id}")
    public Result<List<Brand>> findByCategoryId(@PathVariable("id") Long categoryId) {
        return Result.success(brandService.findByCategoryId(categoryId));
    }

    @PostMapping("/search")
    public Result<List<Brand>> search(@RequestBody Brand brand) {
        return Result.success(brandService.findList(brand));
    }

    @GetMapping("/findAll")
    public Result<List<Brand>> findAll() {
        return Result.success(brandService.findAll());
    }

    @GetMapping("/search/{page}/{size}")
    public Result<PageInfo<Brand>> page(@PathVariable("page") Integer page,
                                        @PathVariable("size") Integer size) {
        return Result.success(brandService.page(page, size));
    }

    @PostMapping("/search/{page}/{size}")
    public Result<PageInfo<Brand>> page(@PathVariable("page") Integer page,
                                        @PathVariable("size") Integer size,
                                        @RequestBody Brand brand) {
        return Result.success(brandService.page(page, size, brand));
    }

}
