package com.changgou.goods.service;

import com.changgou.goods.pojo.Template;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @author tcwgq
 * @since 2020/9/14 17:57
 */
public interface TemplateService {
    int add(Template template);

    int delete(Long id);

    int updateById(Long id, Template template);

    int update(Template template);

    Template findById(Long id);

    List<Template> findList(Template template);

    List<Template> findAll();

    PageInfo<Template> page(Integer page, Integer size);

    PageInfo<Template> page(Integer page, Integer size, Template template);
}
