package com.changgou.goods.service.impl;

import com.changgou.goods.dao.TemplateMapper;
import com.changgou.goods.pojo.Template;
import com.changgou.goods.service.TemplateService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tcwgq
 * @since 2020/9/14 17:57
 */
@Service
public class TemplateServiceImpl implements TemplateService {
    @Resource
    private TemplateMapper templateMapper;

    @Override
    public int add(Template template) {
        return templateMapper.insertSelective(template);
    }

    @Override
    public int delete(Long id) {
        return templateMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int updateById(Long id, Template template) {
        template.setId(id);
        return templateMapper.updateByPrimaryKeySelective(template);
    }

    @Override
    public int update(Template template) {
        return templateMapper.updateByPrimaryKeySelective(template);
    }

    @Override
    public Template findById(Long id) {
        return templateMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Template> findList(Template template) {
        Example example = buildExample(template);
        return templateMapper.selectByExample(example);
    }

    @Override
    public List<Template> findAll() {
        return templateMapper.selectAll();
    }

    @Override
    public PageInfo<Template> page(Integer page, Integer size) {
        PageHelper.startPage(page, size);
        List<Template> templates = templateMapper.selectAll();
        return new PageInfo<>(templates);
    }

    @Override
    public PageInfo<Template> page(Integer page, Integer size, Template template) {
        PageHelper.startPage(page, size);
        Example example = buildExample(template);
        List<Template> templates = templateMapper.selectByExample(example);
        return new PageInfo<>(templates);
    }

    private Example buildExample(Template template) {
        Example example = new Example(Template.class);
        Example.Criteria criteria = example.createCriteria();
        if (template != null) {
            if (template.getId() != null) {
                criteria.andEqualTo("id", template.getId());
            }
            if (StringUtils.isNotBlank(template.getName())) {
                criteria.andLike("name", "%" + template.getName() + "%");
            }
        }
        return example;
    }

}
