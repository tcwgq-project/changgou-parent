package com.changgou.goods.controller;

import com.changgou.common.response.Result;
import com.changgou.goods.pojo.Para;
import com.changgou.goods.service.ParaService;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tcwgq
 * @since 2022/5/27 17:58
 */
@RestController
@RequestMapping("/para")
@CrossOrigin
public class ParaController {
    @Resource
    private ParaService paraService;

    @PostMapping("/add")
    public Result<Integer> add(@RequestBody Para para) {
        return Result.success(paraService.add(para));
    }

    @DeleteMapping("/{id}")
    public Result<Integer> delete(@PathVariable("id") Long id) {
        return Result.success(paraService.delete(id));
    }

    @PutMapping("/{id}")
    public Result<Integer> updateById(@PathVariable("id") Long id, @RequestBody Para para) {
        return Result.success(paraService.updateById(id, para));
    }

    @PostMapping("/update")
    public Result<Integer> update(@RequestBody Para para) {
        return Result.success(paraService.update(para));
    }

    @GetMapping("/{id}")
    public Result<Para> findById(@PathVariable("id") Long id) {
        return Result.success(paraService.findById(id));
    }

    @GetMapping("/category/{id}")
    public Result<List<Para>> findByCategoryId(@PathVariable("id") Long categoryId) {
        return Result.success(paraService.findByCategoryId(categoryId));
    }

    @PostMapping("/search")
    public Result<List<Para>> search(@RequestBody Para para) {
        return Result.success(paraService.findList(para));
    }

    @GetMapping("/search/{page}/{size}")
    public Result<PageInfo<Para>> page(@PathVariable("page") Integer page,
                                       @PathVariable("size") Integer size) {
        return Result.success(paraService.page(page, size));
    }

    @PostMapping("/search/{page}/{size}")
    public Result<PageInfo<Para>> page(@PathVariable("page") Integer page,
                                       @PathVariable("size") Integer size,
                                       @RequestBody Para para) {
        return Result.success(paraService.page(page, size, para));
    }

    @GetMapping("/findAll")
    public Result<List<Para>> findAll() {
        return Result.success(paraService.findAll());
    }

}
