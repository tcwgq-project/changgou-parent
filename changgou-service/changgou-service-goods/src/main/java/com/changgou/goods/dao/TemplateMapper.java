package com.changgou.goods.dao;

import com.changgou.goods.pojo.Template;
import tk.mybatis.mapper.common.Mapper;

/**
 * 通用mapper实现
 *
 * @author tcwgq
 * @since 2020/9/14 17:57
 */
public interface TemplateMapper extends Mapper<Template> {

}
