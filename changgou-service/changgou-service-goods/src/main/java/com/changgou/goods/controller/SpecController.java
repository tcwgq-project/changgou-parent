package com.changgou.goods.controller;

import com.changgou.common.response.Result;
import com.changgou.goods.pojo.Spec;
import com.changgou.goods.service.SpecService;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tcwgq
 * @since 2022/5/27 17:58
 */
@RestController
@RequestMapping("/spec")
@CrossOrigin
public class SpecController {
    @Resource
    private SpecService specService;

    @PostMapping("/add")
    public Result<Integer> add(@RequestBody Spec spec) {
        return Result.success(specService.add(spec));
    }

    @DeleteMapping("/{id}")
    public Result<Integer> delete(@PathVariable("id") Long id) {
        return Result.success(specService.delete(id));
    }

    @PutMapping("/{id}")
    public Result<Integer> updateById(@PathVariable("id") Long id, @RequestBody Spec spec) {
        return Result.success(specService.updateById(id, spec));
    }

    @PostMapping("/update")
    public Result<Integer> update(@RequestBody Spec spec) {
        return Result.success(specService.update(spec));
    }

    @GetMapping("/{id}")
    public Result<Spec> findById(@PathVariable("id") Long id) {
        return Result.success(specService.findById(id));
    }

    @GetMapping("/category/{id}")
    public Result<List<Spec>> findByCategoryId(@PathVariable("id") Long id) {
        return Result.success(specService.findByCategoryId(id));
    }

    @GetMapping("/findAll")
    public Result<List<Spec>> findAll() {
        return Result.success(specService.findAll());
    }

    @PostMapping("/search")
    public Result<List<Spec>> search(@RequestBody Spec spec) {
        return Result.success(specService.findList(spec));
    }

    @GetMapping("/search/{page}/{size}")
    public Result<PageInfo<Spec>> page(@PathVariable("page") Integer page,
                                       @PathVariable("size") Integer size) {
        return Result.success(specService.page(page, size));
    }

    @PostMapping("/search/{page}/{size}")
    public Result<PageInfo<Spec>> page(@PathVariable("page") Integer page,
                                       @PathVariable("size") Integer size,
                                       @RequestBody Spec spec) {
        return Result.success(specService.page(page, size, spec));
    }

}
