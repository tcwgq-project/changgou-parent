package com.changgou.pay.wx;

import com.alibaba.fastjson.JSON;
import com.github.wxpay.sdk.WXPayUtil;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author tcwgq
 * @since 2022/6/18 18:54
 */
public class WxPayTest {
    @Test
    public void test1() throws Exception {
        Map<String, String> params = new HashMap<>();
        Map<String, String> attach = new HashMap<>();
        attach.put("username", "zhangSan");
        attach.put("exchange", "order_pay_notify_exchange");
        attach.put("queue", "order_pay_notify_queue");
        attach.put("routingKey", "");
        params.put("appid", "wx2421b1c4370ec43b");
        params.put("bank_type", "CFT");
        params.put("nonce_str", "5d2b6c2a8db53831f7eda20af46e531c");
        params.put("attach", JSON.toJSONString(attach));
        String content = WXPayUtil.generateSignedXml(params, "hello");
        System.out.println(content);
    }

}
