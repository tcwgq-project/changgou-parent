package com.changgou.pay.mq;

import com.changgou.pay.config.RabbitMQOrderConfig;
import com.changgou.pay.config.RabbitMQSeckillOrderConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author tcwgq
 * @since 2022/6/16 16:39
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class MqTest {
    @Resource
    private RabbitTemplate rabbitTemplate;

    @Test
    public void sendOrder() {
        rabbitTemplate.convertAndSend(
                RabbitMQOrderConfig.ORDER_PAY_NOTIFY_EXCHANGE,
                "",
                "Order message: " + "Hello, world!");
    }

    @Test
    public void sendSeckillOrder() {
        rabbitTemplate.convertAndSend(
                RabbitMQSeckillOrderConfig.ORDER_PAY_NOTIFY_EXCHANGE,
                "",
                "Seckill Order message: " + "Hello, world!");
    }

}
