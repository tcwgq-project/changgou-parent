package com.changgou.pay.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;

// @Configuration
public class RabbitMQSeckillOrderConfig {
    // 定义交换机名称
    public static final String ORDER_PAY_NOTIFY_EXCHANGE = "seckill_order_pay_notify_exchange";
    // 定义队列名称
    public static final String ORDER_PAY_NOTIFY_QUEUE = "seckill_order_pay_notify_queue";

    // 声明队列
    @Bean(ORDER_PAY_NOTIFY_QUEUE)
    public Queue queue() {
        return new Queue(ORDER_PAY_NOTIFY_QUEUE);
    }

    // 声明交换机
    @Bean(ORDER_PAY_NOTIFY_EXCHANGE)
    public Exchange exchange() {
        return ExchangeBuilder.fanoutExchange(ORDER_PAY_NOTIFY_EXCHANGE).durable(true).build();
    }

    // 队列与交换机的绑定
    @Bean("seckillOrderBinding")
    public Binding binding(@Qualifier(ORDER_PAY_NOTIFY_QUEUE) Queue queue, @Qualifier(ORDER_PAY_NOTIFY_EXCHANGE) Exchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("").noargs();
    }

}