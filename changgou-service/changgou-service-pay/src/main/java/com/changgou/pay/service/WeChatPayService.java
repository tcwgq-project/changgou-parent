package com.changgou.pay.service;

import java.util.Map;

/**
 * 描述
 *
 * @author www.itheima.com
 * @version 1.0
 * @package com.changgou.pay.service *
 * @since 1.0
 */
public interface WeChatPayService {
    /**
     * 生成二维码
     *
     * @param out_trade_no 交易流水号
     * @param total_fee    支付金额
     * @return
     */
    Map<String, String> createNative(String out_trade_no, String total_fee, Map<String, String> attach);

    /**
     * @param out_trade_no 交易流水号
     * @return
     */
    Map<String, String> queryStatus(String out_trade_no);

    void sendMsg(String exchange, String routingKey, Object message);

    Map<String, String> close(String out_trade_no);

}
