package com.changgou.pay.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.changgou.common.entity.StatusCode;
import com.changgou.common.response.Result;
import com.changgou.pay.service.WeChatPayService;
import com.github.wxpay.sdk.WXPayUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 描述
 *
 * @version 1.0
 * @since 1.0
 */
@Slf4j
@RestController
@RequestMapping("/wechat/pay")
public class WeChatPayController {
    @Autowired
    private WeChatPayService weChatPayService;

    /**
     * 创建二维码连接地址返回给前端 生成二维码图片
     *
     * @param out_trade_no 外部订单号
     * @param total_fee    金额
     */
    @PostMapping("/create/native")
    public Result<Map<String, String>> createNative(@RequestParam("out_trade_no") String out_trade_no,
                                                    @RequestParam("total_fee") String total_fee,
                                                    @RequestBody Map<String, String> attach) {
        // 获取用户名
        Map<String, String> resultMap = weChatPayService.createNative(out_trade_no, total_fee, attach);
        return new Result<>(true, StatusCode.OK, "二维码连接地址创建成功", resultMap);
    }

    /**
     * 根据交易订单号 来查询订单的状态
     *
     * @param out_trade_no 外部订单号
     */
    @PostMapping("/status/query")
    public Result<Map<String, String>> queryStatus(@RequestParam("out_trade_no") String out_trade_no) {
        Map<String, String> resultMap = weChatPayService.queryStatus(out_trade_no);
        return new Result<>(true, StatusCode.OK, "查询状态OK", resultMap);
    }

    /**
     * 关单
     *
     * @param out_trade_no 外部订单号
     * @return
     */
    @PostMapping("/close")
    public Result<Map<String, String>> close(@RequestParam("out_trade_no") String out_trade_no) {
        Map<String, String> resultMap = weChatPayService.close(out_trade_no);
        return new Result<>(true, StatusCode.OK, "查询状态OK", resultMap);
    }

    /**
     * 接收 微信支付通知的结果  结果(以流的形式传递过来)
     */
    @PostMapping("/notify/url")
    public String notify(HttpServletRequest request) {
        try {
            // 1.获取流信息
            String resultStrXML = readString(request);

            // 2.转成MAP
            Map<String, String> map = WXPayUtil.xmlToMap(resultStrXML);
            log.info("支付回调结果: {}", map);

            String attach = map.get("attach");
            Map<String, String> attachMap = JSON.parseObject(attach, new TypeReference<Map<String, String>>() {
            });
            log.info("通知中收到的attach的数据是: {}", attachMap);

            // 3.发送回调消息
            weChatPayService.sendMsg(attachMap.get("exchange"), attachMap.get("routingKey"), map);

            // 4.返回微信的接收请况(XML的字符串)
            return buildResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String readString(HttpServletRequest request) throws IOException {
        ServletInputStream ins = request.getInputStream();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len;
        while ((len = ins.read(buffer)) != -1) {
            bos.write(buffer, 0, len);
        }
        bos.close();
        ins.close();
        return bos.toString("utf-8");
    }

    private String buildResult() throws Exception {
        Map<String, String> resultMap = new HashMap<>();
        resultMap.put("return_code", "SUCCESS");
        resultMap.put("return_msg", "OK");
        return WXPayUtil.mapToXml(resultMap);
    }

}
