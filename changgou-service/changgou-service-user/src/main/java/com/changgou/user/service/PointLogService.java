package com.changgou.user.service;

import com.changgou.user.bean.PointLog;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * PointLogService
 *
 * @author tcwgq
 * @since 2022/06/25 10:01
 */
public interface PointLogService {
    /**
     * 新增PointLog
     *
     * @param pointLog
     */
    void add(PointLog pointLog);

    /**
     * 删除PointLog
     *
     * @param id
     */
    void delete(String id);

    /**
     * 修改PointLog数据
     *
     * @param pointLog
     */
    void update(PointLog pointLog);

    /**
     * 根据ID查询PointLog
     *
     * @param id
     * @return
     */
    PointLog findById(String id);

    /**
     * 查询所有PointLog
     *
     * @return
     */
    List<PointLog> findAll();

    /**
     * PointLog多条件搜索方法
     *
     * @param pointLog
     * @return
     */
    List<PointLog> findList(PointLog pointLog);

    /**
     * PointLog分页查询
     *
     * @param page
     * @param size
     * @return
     */
    PageInfo<PointLog> findPage(int page, int size);

    /**
     * PointLog多条件分页查询
     *
     * @param pointLog
     * @param page
     * @param size
     * @return
     */
    PageInfo<PointLog> findPage(PointLog pointLog, int page, int size);

}
