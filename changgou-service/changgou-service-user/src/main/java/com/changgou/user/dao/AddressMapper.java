package com.changgou.user.dao;

import com.changgou.user.bean.Address;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * AddressDao
 *
 * @author tcwgq
 * @since 2022/06/12 21:40
 */
public interface AddressMapper extends Mapper<Address> {
    @Select("select * from tb_address where username = #{username}")
    List<Address> findByUsername(@Param("username") String username);

}
