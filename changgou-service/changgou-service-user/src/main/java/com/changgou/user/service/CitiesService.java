package com.changgou.user.service;

import com.changgou.user.bean.Cities;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * CitiesService
 *
 * @author tcwgq
 * @since 2022/06/12 21:40
 */
public interface CitiesService {
    /**
     * 新增Cities
     *
     * @param cities
     */
    void add(Cities cities);

    /**
     * 删除Cities
     *
     * @param id
     */
    void delete(String id);

    /**
     * 修改Cities数据
     *
     * @param cities
     */
    void update(Cities cities);

    /**
     * 根据ID查询Cities
     *
     * @param id
     * @return
     */
    Cities findById(String id);

    /**
     * 查询所有Cities
     *
     * @return
     */
    List<Cities> findAll();

    /**
     * Cities多条件搜索方法
     *
     * @param cities
     * @return
     */
    List<Cities> findList(Cities cities);

    /**
     * Cities分页查询
     *
     * @param page
     * @param size
     * @return
     */
    PageInfo<Cities> findPage(int page, int size);

    /**
     * Cities多条件分页查询
     *
     * @param cities
     * @param page
     * @param size
     * @return
     */
    PageInfo<Cities> findPage(Cities cities, int page, int size);

}
