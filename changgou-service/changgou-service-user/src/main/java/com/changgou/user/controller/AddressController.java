package com.changgou.user.controller;

import com.changgou.common.entity.StatusCode;
import com.changgou.common.response.Result;
import com.changgou.common_spring.utils.TokenDecoder;
import com.changgou.user.bean.Address;
import com.changgou.user.service.AddressService;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * AddressController
 *
 * @author tcwgq
 * @since 2022/06/12 21:40
 */
@RestController
@RequestMapping("/address")
@CrossOrigin
public class AddressController {
    @Resource
    private AddressService addressService;

    /**
     * 新增Address数据
     *
     * @param address
     * @return
     */
    @PostMapping
    public Result<String> add(@RequestBody Address address) {
        // 调用AddressService实现添加Address
        addressService.add(address);
        return new Result<>(true, StatusCode.OK, "添加成功");
    }

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    public Result<String> delete(@PathVariable Integer id) {
        // 调用AddressService实现根据主键删除
        addressService.delete(id);
        return new Result<>(true, StatusCode.OK, "删除成功");
    }

    /**
     * 修改Address数据
     *
     * @param address
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    public Result<String> update(@RequestBody Address address, @PathVariable Integer id) {
        // 设置主键值
        address.setId(id);
        // 调用AddressService实现修改Address
        addressService.update(address);
        return new Result<>(true, StatusCode.OK, "修改成功");
    }

    /**
     * 根据ID查询Address数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<Address> findById(@PathVariable Integer id) {
        // 调用AddressService实现根据主键查询Address
        Address address = addressService.findById(id);
        return new Result<>(true, StatusCode.OK, "查询成功", address);
    }

    /**
     * 根据username查询Address数据
     */
    @GetMapping("/user/list")
    public Result<List<Address>> findByUsername() {
        String username = TokenDecoder.getUsername();
        // 调用AddressService实现根据主键查询Address
        List<Address> addresses = addressService.findByUsername(username);
        return new Result<>(true, StatusCode.OK, "查询成功", addresses);
    }

    /**
     * 查询Address全部数据
     *
     * @return
     */
    @GetMapping
    public Result<List<Address>> findAll() {
        // 调用AddressService实现查询所有Address
        List<Address> list = addressService.findAll();
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    /**
     * 多条件搜索品牌数据
     *
     * @param address
     * @return
     */
    @PostMapping(value = "/search")
    public Result<List<Address>> findList(@RequestBody(required = false) Address address) {
        // 调用AddressService实现条件查询Address
        List<Address> list = addressService.findList(address);
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    /**
     * Address分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<Address>> findPage(@PathVariable int page, @PathVariable int size) {
        // 调用AddressService实现分页查询Address
        PageInfo<Address> pageInfo = addressService.findPage(page, size);
        return new Result<>(true, StatusCode.OK, "查询成功", pageInfo);
    }

    /**
     * Address分页条件搜索实现
     *
     * @param address
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<Address>> findPage(@RequestBody(required = false) Address address, @PathVariable int page, @PathVariable int size) {
        // 调用AddressService实现分页条件查询Address
        PageInfo<Address> pageInfo = addressService.findPage(address, page, size);
        return new Result<>(true, StatusCode.OK, "查询成功", pageInfo);
    }

}
