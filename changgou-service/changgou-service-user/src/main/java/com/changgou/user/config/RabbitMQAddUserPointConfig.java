package com.changgou.user.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQAddUserPointConfig {
    // 添加积分任务交换机
    public static final String EX_BUYING_ADD_POINT_USER = "buying_add_point_user_exchange";

    // 添加积分消息队列
    public static final String QU_BUYING_ADD_POINT = "buying_add_point_queue";

    // 完成添加积分消息队列
    public static final String QU_BUYING_FINISH_ADD_POINT = "buying_finish_add_point_queue";

    // 添加积分路由key
    public static final String KEY_BUYING_ADD_POINT_KEY = "add_point";

    // 完成添加积分路由key
    public static final String KEY_BUYING_FINISH_ADD_POINT_KEY = "finish_add_point";

    /**
     * 交换机配置
     */
    @Bean(EX_BUYING_ADD_POINT_USER)
    public Exchange exchange() {
        return ExchangeBuilder.directExchange(EX_BUYING_ADD_POINT_USER).durable(true).build();
    }

    // 声明队列
    @Bean(QU_BUYING_FINISH_ADD_POINT)
    public Queue queue1() {
        return new Queue(QU_BUYING_FINISH_ADD_POINT);
    }

    // 声明队列
    @Bean(QU_BUYING_ADD_POINT)
    public Queue queue2() {
        return new Queue(QU_BUYING_ADD_POINT);
    }

    /**
     * 绑定队列到交换机
     */
    @Bean(QU_BUYING_FINISH_ADD_POINT + "_" + EX_BUYING_ADD_POINT_USER)
    public Binding binding1(@Qualifier(QU_BUYING_FINISH_ADD_POINT) Queue queue,
                            @Qualifier(EX_BUYING_ADD_POINT_USER) Exchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(KEY_BUYING_FINISH_ADD_POINT_KEY).noargs();
    }

    @Bean(QU_BUYING_ADD_POINT + "_" + EX_BUYING_ADD_POINT_USER)
    public Binding binding2(@Qualifier(QU_BUYING_ADD_POINT) Queue queue,
                            @Qualifier(EX_BUYING_ADD_POINT_USER) Exchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(KEY_BUYING_ADD_POINT_KEY).noargs();
    }

}