package com.changgou.user.controller;

import com.alibaba.fastjson.JSON;
import com.changgou.common.entity.StatusCode;
import com.changgou.common.response.Result;
import com.changgou.common.utils.BCrypt;
import com.changgou.common.utils.JwtUtil;
import com.changgou.user.bean.User;
import com.changgou.user.service.UserService;
import com.github.pagehelper.PageInfo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * UserController
 *
 * @author tcwgq
 * @since 2022/06/12 21:40
 */
@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {
    @Resource
    private UserService userService;

    @PostMapping("/login")
    public Result<String> login(@RequestParam String username, @RequestParam String password, HttpServletResponse response) {
        User user = userService.findById(username);

        if (user != null && BCrypt.checkpw(password, user.getPassword())) {
            Map<String, Object> map = new HashMap<>();
            map.put("role", "user");
            map.put("username", user.getUsername());
            String token = JwtUtil.createJWT(UUID.randomUUID().toString(), JSON.toJSONString(map), null);
            Cookie cookie = new Cookie("access_token", token);
            cookie.setDomain("localhost");
            cookie.setPath("/");
            response.addCookie(cookie);
            return new Result<>(true, StatusCode.OK, "登陆成功", token);
        }

        return Result.fail(StatusCode.LOGIN_ERROR, "用户名或密码错误");
    }

    /**
     * 添加积分
     *
     * @param points
     * @param username
     * @return
     */
    @PostMapping("/points/add")
    public Result<String> addPoints(@RequestParam(value = "points") Integer points,
                                    @RequestParam(value = "username") String username) {

        userService.addPoints(points, username);
        return new Result<>(true, StatusCode.OK, "添加积分成功");
    }

    /**
     * 新增User数据
     *
     * @param user
     * @return
     */
    @PostMapping
    public Result<String> add(@RequestBody User user) {
        // 调用UserService实现添加User
        userService.add(user);
        return new Result<>(true, StatusCode.OK, "添加成功");
    }

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    public Result<String> delete(@PathVariable String id) {
        // 调用UserService实现根据主键删除
        userService.delete(id);
        return new Result<>(true, StatusCode.OK, "删除成功");
    }

    /**
     * 修改User数据
     *
     * @param user
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    public Result<String> update(@RequestBody User user, @PathVariable String id) {
        // 设置主键值
        user.setUsername(id);
        // 调用UserService实现修改User
        userService.update(user);
        return new Result<>(true, StatusCode.OK, "修改成功");
    }

    /**
     * 根据ID查询User数据
     *
     * @param id
     * @return
     */
    @GetMapping({"/{id}", "/load/{id}"})
    public Result<User> findById(@PathVariable String id) {
        // 调用UserService实现根据主键查询User
        User user = userService.findById(id);
        return new Result<>(true, StatusCode.OK, "查询成功", user);
    }

    /**
     * 权限控制
     * 查询User全部数据
     *
     * @return
     */
    @PreAuthorize(value = "hasAuthority('admin')")
    @GetMapping
    public Result<List<User>> findAll() {
        // 调用UserService实现查询所有User
        List<User> list = userService.findAll();
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    /**
     * 多条件搜索品牌数据
     *
     * @param user
     * @return
     */
    @PostMapping(value = "/search")
    public Result<List<User>> findList(@RequestBody(required = false) User user) {
        // 调用UserService实现条件查询User
        List<User> list = userService.findList(user);
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    /**
     * User分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<User>> findPage(@PathVariable int page, @PathVariable int size) {
        // 调用UserService实现分页查询User
        PageInfo<User> pageInfo = userService.findPage(page, size);
        return new Result<>(true, StatusCode.OK, "查询成功", pageInfo);
    }

    /**
     * User分页条件搜索实现
     *
     * @param user
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<User>> findPage(@RequestBody(required = false) User user, @PathVariable int page, @PathVariable int size) {
        // 调用UserService实现分页条件查询User
        PageInfo<User> pageInfo = userService.findPage(user, page, size);
        return new Result<>(true, StatusCode.OK, "查询成功", pageInfo);
    }

}
