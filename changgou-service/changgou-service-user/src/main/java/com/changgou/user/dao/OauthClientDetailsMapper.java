package com.changgou.user.dao;

import com.changgou.user.bean.OauthClientDetails;
import tk.mybatis.mapper.common.Mapper;

/**
 * OauthClientDetailsDao
 *
 * @author tcwgq
 * @since 2022/06/12 21:40
 */
public interface OauthClientDetailsMapper extends Mapper<OauthClientDetails> {

}
