package com.changgou.user.service;

import com.changgou.order.bean.Task;
import com.changgou.user.bean.User;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * UserService
 *
 * @author tcwgq
 * @since 2022/06/12 21:40
 */
public interface UserService {
    int addPoints(Integer points, String username);

    /**
     * 新增User
     *
     * @param user
     */
    void add(User user);

    /**
     * 删除User
     *
     * @param id
     */
    void delete(String id);

    /**
     * 修改User数据
     *
     * @param user
     */
    void update(User user);

    int updateUserPoints(Task task);

    /**
     * 根据ID查询User
     *
     * @param id
     * @return
     */
    User findById(String id);

    /**
     * 查询所有User
     *
     * @return
     */
    List<User> findAll();

    /**
     * User多条件搜索方法
     *
     * @param user
     * @return
     */
    List<User> findList(User user);

    /**
     * User分页查询
     *
     * @param page
     * @param size
     * @return
     */
    PageInfo<User> findPage(int page, int size);

    /**
     * User多条件分页查询
     *
     * @param user
     * @param page
     * @param size
     * @return
     */
    PageInfo<User> findPage(User user, int page, int size);

}
