package com.changgou.user.dao;

import com.changgou.user.bean.UndoLog;
import tk.mybatis.mapper.common.Mapper;

/**
 * UndoLogDao
 *
 * @author tcwgq
 * @since 2022/06/12 21:40
 */
public interface UndoLogMapper extends Mapper<UndoLog> {

}
