package com.changgou.user.dao;

import com.changgou.user.bean.Provinces;
import tk.mybatis.mapper.common.Mapper;

/**
 * ProvincesDao
 *
 * @author tcwgq
 * @since 2022/06/12 21:40
 */
public interface ProvincesMapper extends Mapper<Provinces> {

}
