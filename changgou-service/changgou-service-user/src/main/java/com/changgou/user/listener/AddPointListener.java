package com.changgou.user.listener;

import com.alibaba.fastjson.JSON;
import com.changgou.order.bean.Task;
import com.changgou.user.config.RabbitMQAddUserPointConfig;
import com.changgou.user.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class AddPointListener {
    @Autowired
    private UserService userService;
    @Resource(name = "taskRedisTemplate")
    private RedisTemplate<String, Task> redisTemplate;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @RabbitListener(queues = RabbitMQAddUserPointConfig.QU_BUYING_ADD_POINT)
    public void receiveMessage(String message) {
        Task task = JSON.parseObject(message, Task.class);
        if (task == null || StringUtils.isEmpty(task.getRequestBody())) {
            return;
        }
        // 判断redis中是否存在内容
        Task value = redisTemplate.boundValueOps("task:" + task.getId()).get();
        if (value != null) {
            return;
        }
        // 更新用户积分
        int result = userService.updateUserPoints(task);
        if (result <= 0) {
            return;
        }
        // 返回通知
        rabbitTemplate.convertAndSend(
                RabbitMQAddUserPointConfig.EX_BUYING_ADD_POINT_USER,
                RabbitMQAddUserPointConfig.KEY_BUYING_FINISH_ADD_POINT_KEY,
                JSON.toJSONString(task));
    }

}