package com.changgou.user.dao;

import com.changgou.user.bean.Cities;
import tk.mybatis.mapper.common.Mapper;

/**
 * CitiesDao
 *
 * @author tcwgq
 * @since 2022/06/12 21:40
 */
public interface CitiesMapper extends Mapper<Cities> {

}
