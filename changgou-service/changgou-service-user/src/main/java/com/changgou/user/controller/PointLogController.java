package com.changgou.user.controller;

import com.changgou.common.entity.StatusCode;
import com.changgou.common.response.Result;
import com.changgou.user.bean.PointLog;
import com.changgou.user.service.PointLogService;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * PointLogController
 *
 * @author tcwgq
 * @since 2022/06/25 10:01
 */
@RestController
@RequestMapping("/pointLog")
@CrossOrigin
public class PointLogController {
    @Resource
    private PointLogService pointLogService;

    /**
     * 新增PointLog数据
     *
     * @param pointLog
     * @return
     */
    @PostMapping
    public Result<String> add(@RequestBody PointLog pointLog) {
        // 调用PointLogService实现添加PointLog
        pointLogService.add(pointLog);
        return new Result<>(true, StatusCode.OK, "添加成功");
    }

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    public Result<String> delete(@PathVariable String id) {
        // 调用PointLogService实现根据主键删除
        pointLogService.delete(id);
        return new Result<>(true, StatusCode.OK, "删除成功");
    }

    /**
     * 修改PointLog数据
     *
     * @param pointLog
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    public Result<String> update(@RequestBody PointLog pointLog, @PathVariable String id) {
        // 设置主键值
        pointLog.setOrderId(id);
        // 调用PointLogService实现修改PointLog
        pointLogService.update(pointLog);
        return new Result<>(true, StatusCode.OK, "修改成功");
    }

    /**
     * 根据ID查询PointLog数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<PointLog> findById(@PathVariable String id) {
        // 调用PointLogService实现根据主键查询PointLog
        PointLog pointLog = pointLogService.findById(id);
        return new Result<>(true, StatusCode.OK, "查询成功", pointLog);
    }

    /**
     * 查询PointLog全部数据
     *
     * @return
     */
    @GetMapping
    public Result<List<PointLog>> findAll() {
        // 调用PointLogService实现查询所有PointLog
        List<PointLog> list = pointLogService.findAll();
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    /**
     * 多条件搜索品牌数据
     *
     * @param pointLog
     * @return
     */
    @PostMapping(value = "/search")
    public Result<List<PointLog>> findList(@RequestBody(required = false) PointLog pointLog) {
        // 调用PointLogService实现条件查询PointLog
        List<PointLog> list = pointLogService.findList(pointLog);
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    /**
     * PointLog分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<PointLog>> findPage(@PathVariable int page, @PathVariable int size) {
        // 调用PointLogService实现分页查询PointLog
        PageInfo<PointLog> pageInfo = pointLogService.findPage(page, size);
        return new Result<>(true, StatusCode.OK, "查询成功", pageInfo);
    }

    /**
     * PointLog分页条件搜索实现
     *
     * @param pointLog
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<PointLog>> findPage(@RequestBody(required = false) PointLog pointLog, @PathVariable int page, @PathVariable int size) {
        // 调用PointLogService实现分页条件查询PointLog
        PageInfo<PointLog> pageInfo = pointLogService.findPage(pointLog, page, size);
        return new Result<>(true, StatusCode.OK, "查询成功", pageInfo);
    }

}
