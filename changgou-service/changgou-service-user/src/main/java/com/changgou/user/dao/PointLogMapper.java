package com.changgou.user.dao;

import com.changgou.user.bean.PointLog;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

/**
 * PointLogDao
 *
 * @author tcwgq
 * @since 2022/06/25 10:01
 */
public interface PointLogMapper extends Mapper<PointLog> {
    @Select("select * from tb_point_log where order_id = #{orderId}")
    PointLog findLogInfoByOrderId(@Param("orderId") String orderId);

}
