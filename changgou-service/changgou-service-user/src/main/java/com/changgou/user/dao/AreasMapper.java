package com.changgou.user.dao;

import com.changgou.user.bean.Areas;
import tk.mybatis.mapper.common.Mapper;

/**
 * AreasDao
 *
 * @author tcwgq
 * @since 2022/06/12 21:40
 */
public interface AreasMapper extends Mapper<Areas> {

}
