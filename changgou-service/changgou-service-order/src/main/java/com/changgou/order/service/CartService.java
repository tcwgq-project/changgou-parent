package com.changgou.order.service;

import com.changgou.order.bean.OrderItem;

import java.util.List;

/**
 * @author tcwgq
 * @since 2022/6/14 16:04
 */
public interface CartService {
    void add(Long skuId, Integer num, String username);

    List<OrderItem> list(String username);

    List<OrderItem> filter(String username, List<Long> ids);

    void remove(String username, List<Long> ids);
}
