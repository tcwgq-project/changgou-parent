package com.changgou.order.controller;

import com.changgou.common.entity.StatusCode;
import com.changgou.common.response.Result;
import com.changgou.order.bean.TaskHis;
import com.changgou.order.service.TaskHisService;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * TaskHisController
 *
 * @author tcwgq
 * @since 2022/06/25 10:01
 */
@RestController
@RequestMapping("/taskHis")
@CrossOrigin
public class TaskHisController {
    @Resource
    private TaskHisService taskHisService;

    /**
     * 新增TaskHis数据
     *
     * @param taskHis
     * @return
     */
    @PostMapping
    public Result<String> add(@RequestBody TaskHis taskHis) {
        // 调用TaskHisService实现添加TaskHis
        taskHisService.add(taskHis);
        return new Result<>(true, StatusCode.OK, "添加成功");
    }

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    public Result<String> delete(@PathVariable Long id) {
        // 调用TaskHisService实现根据主键删除
        taskHisService.delete(id);
        return new Result<>(true, StatusCode.OK, "删除成功");
    }

    /**
     * 修改TaskHis数据
     *
     * @param taskHis
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    public Result<String> update(@RequestBody TaskHis taskHis, @PathVariable Long id) {
        // 设置主键值
        taskHis.setId(id);
        // 调用TaskHisService实现修改TaskHis
        taskHisService.update(taskHis);
        return new Result<>(true, StatusCode.OK, "修改成功");
    }

    /**
     * 根据ID查询TaskHis数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<TaskHis> findById(@PathVariable Long id) {
        // 调用TaskHisService实现根据主键查询TaskHis
        TaskHis taskHis = taskHisService.findById(id);
        return new Result<>(true, StatusCode.OK, "查询成功", taskHis);
    }

    /**
     * 查询TaskHis全部数据
     *
     * @return
     */
    @GetMapping
    public Result<List<TaskHis>> findAll() {
        // 调用TaskHisService实现查询所有TaskHis
        List<TaskHis> list = taskHisService.findAll();
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    /**
     * 多条件搜索品牌数据
     *
     * @param taskHis
     * @return
     */
    @PostMapping(value = "/search")
    public Result<List<TaskHis>> findList(@RequestBody(required = false) TaskHis taskHis) {
        // 调用TaskHisService实现条件查询TaskHis
        List<TaskHis> list = taskHisService.findList(taskHis);
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    /**
     * TaskHis分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<TaskHis>> findPage(@PathVariable int page, @PathVariable int size) {
        // 调用TaskHisService实现分页查询TaskHis
        PageInfo<TaskHis> pageInfo = taskHisService.findPage(page, size);
        return new Result<>(true, StatusCode.OK, "查询成功", pageInfo);
    }

    /**
     * TaskHis分页条件搜索实现
     *
     * @param taskHis
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<TaskHis>> findPage(@RequestBody(required = false) TaskHis taskHis, @PathVariable int page, @PathVariable int size) {
        // 调用TaskHisService实现分页条件查询TaskHis
        PageInfo<TaskHis> pageInfo = taskHisService.findPage(taskHis, page, size);
        return new Result<>(true, StatusCode.OK, "查询成功", pageInfo);
    }

}
