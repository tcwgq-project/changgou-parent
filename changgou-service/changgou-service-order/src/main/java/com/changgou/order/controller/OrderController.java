package com.changgou.order.controller;

import com.changgou.common.entity.StatusCode;
import com.changgou.common.response.Result;
import com.changgou.common_spring.utils.TokenDecoder;
import com.changgou.order.bean.Order;
import com.changgou.order.service.OrderService;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * OrderController
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */

@RestController
@RequestMapping("/order")
@CrossOrigin
public class OrderController {
    @Resource
    private OrderService orderService;

    /**
     * 创建订单
     *
     * @param order
     * @return
     */
    @PostMapping("/create")
    public Result<Order> create(@RequestBody Order order) {
        String username = TokenDecoder.getUsername();
        order.setUsername(username);
        // 调用OrderService实现添加Order
        Order result = orderService.create(order);
        return new Result<>(true, StatusCode.OK, "下单成功", result);
    }

    /**
     * 确认收货
     *
     * @param orderId  订单号
     * @param operator 操作者
     * @return
     */
    @PutMapping("/take/{orderId}/{operator}")
    public Result<String> take(@PathVariable String orderId, @PathVariable String operator) {
        orderService.take(orderId, operator);
        return new Result<>(true, StatusCode.OK, "确认收货成功");
    }

    /**
     * 批量发货
     *
     * @param orders 订单列表
     */
    @PostMapping("/batchSend")
    public Result<String> batchSend(@RequestBody List<Order> orders) {
        orderService.batchSend(orders);
        return new Result<>(true, StatusCode.OK, "批量发货成功");
    }

    /**
     * 自动收货
     */
    @PostMapping("/autoTack")
    public Result<String> autoTack() {
        orderService.autoTack();
        return new Result<>(true, StatusCode.OK, "自动收货成功");
    }

    /**
     * 新增Order数据
     *
     * @param order
     * @return
     */
    @PostMapping
    public Result<String> add(@RequestBody Order order) {
        // 调用OrderService实现添加Order
        orderService.add(order);
        return new Result<>(true, StatusCode.OK, "添加成功");
    }

    /**
     * 根据ID删除品牌数据
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/{id}")
    public Result<String> delete(@PathVariable String id) {
        // 调用OrderService实现根据主键删除
        orderService.delete(id);
        return new Result<>(true, StatusCode.OK, "删除成功");
    }

    /**
     * 修改Order数据
     *
     * @param order
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}")
    public Result<String> update(@RequestBody Order order, @PathVariable String id) {
        // 设置主键值
        order.setId(id);
        // 调用OrderService实现修改Order
        orderService.update(order);
        return new Result<>(true, StatusCode.OK, "修改成功");
    }

    /**
     * 根据ID查询Order数据
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<Order> findById(@PathVariable String id) {
        // 调用OrderService实现根据主键查询Order
        Order order = orderService.findById(id);
        return new Result<>(true, StatusCode.OK, "查询成功", order);
    }

    /**
     * 查询Order全部数据
     *
     * @return
     */
    @GetMapping
    public Result<List<Order>> findAll() {
        // 调用OrderService实现查询所有Order
        List<Order> list = orderService.findAll();
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    /**
     * 多条件搜索品牌数据
     *
     * @param order
     * @return
     */
    @PostMapping(value = "/search")
    public Result<List<Order>> findList(@RequestBody(required = false) Order order) {
        // 调用OrderService实现条件查询Order
        List<Order> list = orderService.findList(order);
        return new Result<>(true, StatusCode.OK, "查询成功", list);
    }

    /**
     * Order分页搜索实现
     *
     * @param page:当前页
     * @param size:每页显示多少条
     * @return
     */
    @GetMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<Order>> findPage(@PathVariable int page, @PathVariable int size) {
        // 调用OrderService实现分页查询Order
        PageInfo<Order> pageInfo = orderService.findPage(page, size);
        return new Result<>(true, StatusCode.OK, "查询成功", pageInfo);
    }

    /**
     * Order分页条件搜索实现
     *
     * @param order
     * @param page
     * @param size
     * @return
     */
    @PostMapping(value = "/search/{page}/{size}")
    public Result<PageInfo<Order>> findPage(@RequestBody(required = false) Order order, @PathVariable int page, @PathVariable int size) {
        // 调用OrderService实现分页条件查询Order
        PageInfo<Order> pageInfo = orderService.findPage(order, page, size);
        return new Result<>(true, StatusCode.OK, "查询成功", pageInfo);
    }

}
