package com.changgou.order.dao;

import com.changgou.order.bean.CategoryReport;
import tk.mybatis.mapper.common.Mapper;

/**
 * CategoryReportDao
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
public interface CategoryReportMapper extends Mapper<CategoryReport> {

}
