package com.changgou.order.service;

import com.changgou.order.bean.Task;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * TaskService
 *
 * @author tcwgq
 * @since 2022/06/25 10:01
 */
public interface TaskService {
    /**
     * 新增Task
     *
     * @param task
     */
    void add(Task task);

    /**
     * 删除Task
     *
     * @param id
     */
    void delete(Long id);

    void delTask(Task task);

    /**
     * 修改Task数据
     *
     * @param task
     */
    void update(Task task);

    /**
     * 根据ID查询Task
     *
     * @param id
     * @return
     */
    Task findById(Long id);

    /**
     * 查询所有Task
     *
     * @return
     */
    List<Task> findAll();

    /**
     * Task多条件搜索方法
     *
     * @param task
     * @return
     */
    List<Task> findList(Task task);

    /**
     * Task分页查询
     *
     * @param page
     * @param size
     * @return
     */
    PageInfo<Task> findPage(int page, int size);

    /**
     * Task多条件分页查询
     *
     * @param task
     * @param page
     * @param size
     * @return
     */
    PageInfo<Task> findPage(Task task, int page, int size);

}
