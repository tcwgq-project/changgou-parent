package com.changgou.order.listener;

import com.changgou.order.config.RabbitMQOrderAutoCloseConfig;
import com.changgou.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * TODO 消息监听时，无法获取请求头中的授权token
 */
@Slf4j
@Component
public class OrderAutoCloseListener {
    @Resource
    private OrderService orderService;

    /**
     * TODO 消息幂等处理
     *
     * @param orderId 消息
     */
    @RabbitListener(queues = RabbitMQOrderAutoCloseConfig.ORDER_AUTO_EXPIRE_DEAD_QUEUE)
    public void receiveMessage(String orderId) {
        log.info("orderId: {}", orderId);
        orderService.close(orderId);
    }

}