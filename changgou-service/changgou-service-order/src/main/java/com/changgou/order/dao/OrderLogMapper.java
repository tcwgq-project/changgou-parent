package com.changgou.order.dao;

import com.changgou.order.bean.OrderLog;
import tk.mybatis.mapper.common.Mapper;

/**
 * OrderLogDao
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
public interface OrderLogMapper extends Mapper<OrderLog> {

}
