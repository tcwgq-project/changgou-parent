package com.changgou.order.dao;

import com.changgou.order.bean.OrderItem;
import tk.mybatis.mapper.common.Mapper;

/**
 * OrderItemDao
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
public interface OrderItemMapper extends Mapper<OrderItem> {

}
