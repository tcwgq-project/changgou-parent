package com.changgou.order.task;

import com.alibaba.fastjson.JSON;
import com.changgou.order.bean.Task;
import com.changgou.order.config.RabbitMQAddUserPointConfig;
import com.changgou.order.dao.TaskMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Component
public class QueryPointTask {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Resource
    private TaskMapper taskMapper;

    @Scheduled(cron = "0 0/2 * * * ?")
    public void queryTask() {
        // 1.获取小于系统当前时间数据
        List<Task> taskList = taskMapper.findTaskLessTanCurrentTime(new Date());
        if (taskList != null && taskList.size() > 0) {
            // 将任务数据发送到消息队列
            for (Task task : taskList) {
                rabbitTemplate.convertAndSend(
                        RabbitMQAddUserPointConfig.EX_BUYING_ADD_POINT_USER,
                        RabbitMQAddUserPointConfig.KEY_BUYING_ADD_POINT_KEY,
                        JSON.toJSONString(task));
            }
        }
    }

}