package com.changgou.order.listener;

import com.changgou.order.config.RabbitMQWeChatPayNotifyConfig;
import com.changgou.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;

@Slf4j
@Component
public class WeChatPayNotifyListener {
    @Resource
    private OrderService orderService;

    /**
     * TODO 消息幂等处理
     *
     * @param message 消息
     */
    @RabbitListener(queues = RabbitMQWeChatPayNotifyConfig.ORDER_PAY_NOTIFY_QUEUE)
    public void receiveMessage(Map<String, String> message) {
        log.info("message: {}", message);
        // 1.接收消息
        // 2.更新对应的订单的状态
        if (message != null) {
            if (message.get("return_code").equalsIgnoreCase("success")) {
                // 格式为yyyyMMddHHmmss
                orderService.update(message.get("out_trade_no"), message.get("transaction_id"),
                        getPayTime(message.get("time_end")));
            } else {
                // 删除订单 支付失败
                orderService.delete(message.get("out_trade_no"), message.get("transaction_id"),
                        getPayTime(message.get("time_end")));
            }
        }
    }

    private Date getPayTime(String time_end) {
        return Date.from(LocalDateTime.parse(time_end, DateTimeFormatter.ofPattern("yyyyMMddHHmmss")).atZone(ZoneId.systemDefault()).toInstant());
    }

}