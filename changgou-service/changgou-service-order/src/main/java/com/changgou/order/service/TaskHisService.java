package com.changgou.order.service;

import com.changgou.order.bean.TaskHis;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * TaskHisService
 *
 * @author tcwgq
 * @since 2022/06/25 10:01
 */
public interface TaskHisService {
    /**
     * 新增TaskHis
     *
     * @param taskHis
     */
    void add(TaskHis taskHis);

    /**
     * 删除TaskHis
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 修改TaskHis数据
     *
     * @param taskHis
     */
    void update(TaskHis taskHis);

    /**
     * 根据ID查询TaskHis
     *
     * @param id
     * @return
     */
    TaskHis findById(Long id);

    /**
     * 查询所有TaskHis
     *
     * @return
     */
    List<TaskHis> findAll();

    /**
     * TaskHis多条件搜索方法
     *
     * @param taskHis
     * @return
     */
    List<TaskHis> findList(TaskHis taskHis);

    /**
     * TaskHis分页查询
     *
     * @param page
     * @param size
     * @return
     */
    PageInfo<TaskHis> findPage(int page, int size);

    /**
     * TaskHis多条件分页查询
     *
     * @param taskHis
     * @param page
     * @param size
     * @return
     */
    PageInfo<TaskHis> findPage(TaskHis taskHis, int page, int size);

}
