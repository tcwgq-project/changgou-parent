package com.changgou.order.service;

import com.changgou.order.bean.UndoLog;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * UndoLogService
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
public interface UndoLogService {
    /**
     * 新增UndoLog
     *
     * @param undoLog
     */
    void add(UndoLog undoLog);

    /**
     * 删除UndoLog
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 修改UndoLog数据
     *
     * @param undoLog
     */
    void update(UndoLog undoLog);

    /**
     * 根据ID查询UndoLog
     *
     * @param id
     * @return
     */
    UndoLog findById(Long id);

    /**
     * 查询所有UndoLog
     *
     * @return
     */
    List<UndoLog> findAll();

    /**
     * UndoLog多条件搜索方法
     *
     * @param undoLog
     * @return
     */
    List<UndoLog> findList(UndoLog undoLog);

    /**
     * UndoLog分页查询
     *
     * @param page
     * @param size
     * @return
     */
    PageInfo<UndoLog> findPage(int page, int size);

    /**
     * UndoLog多条件分页查询
     *
     * @param undoLog
     * @param page
     * @param size
     * @return
     */
    PageInfo<UndoLog> findPage(UndoLog undoLog, int page, int size);

}
