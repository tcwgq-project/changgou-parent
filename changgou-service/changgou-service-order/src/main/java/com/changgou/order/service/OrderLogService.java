package com.changgou.order.service;

import com.changgou.order.bean.OrderLog;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * OrderLogService
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
public interface OrderLogService {
    /**
     * 新增OrderLog
     *
     * @param orderLog
     */
    void add(OrderLog orderLog);

    /**
     * 删除OrderLog
     *
     * @param id
     */
    void delete(String id);

    /**
     * 修改OrderLog数据
     *
     * @param orderLog
     */
    void update(OrderLog orderLog);

    /**
     * 根据ID查询OrderLog
     *
     * @param id
     * @return
     */
    OrderLog findById(String id);

    /**
     * 查询所有OrderLog
     *
     * @return
     */
    List<OrderLog> findAll();

    /**
     * OrderLog多条件搜索方法
     *
     * @param orderLog
     * @return
     */
    List<OrderLog> findList(OrderLog orderLog);

    /**
     * OrderLog分页查询
     *
     * @param page
     * @param size
     * @return
     */
    PageInfo<OrderLog> findPage(int page, int size);

    /**
     * OrderLog多条件分页查询
     *
     * @param orderLog
     * @param page
     * @param size
     * @return
     */
    PageInfo<OrderLog> findPage(OrderLog orderLog, int page, int size);

}
