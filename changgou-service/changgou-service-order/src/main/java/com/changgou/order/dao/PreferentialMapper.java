package com.changgou.order.dao;

import com.changgou.order.bean.Preferential;
import tk.mybatis.mapper.common.Mapper;

/**
 * PreferentialDao
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
public interface PreferentialMapper extends Mapper<Preferential> {

}
