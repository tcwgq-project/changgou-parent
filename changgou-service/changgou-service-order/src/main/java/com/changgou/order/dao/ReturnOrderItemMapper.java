package com.changgou.order.dao;

import com.changgou.order.bean.ReturnOrderItem;
import tk.mybatis.mapper.common.Mapper;

/**
 * ReturnOrderItemDao
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
public interface ReturnOrderItemMapper extends Mapper<ReturnOrderItem> {

}
