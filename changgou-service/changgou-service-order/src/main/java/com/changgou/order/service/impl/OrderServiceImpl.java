package com.changgou.order.service.impl;

import com.alibaba.fastjson.JSON;
import com.changgou.common.entity.StatusCode;
import com.changgou.common.exception.ChanggouErrorCode;
import com.changgou.common.exception.ChanggouException;
import com.changgou.common.response.Result;
import com.changgou.common.utils.IdWorker;
import com.changgou.goods.feign.SkuFeign;
import com.changgou.order.bean.*;
import com.changgou.order.config.RabbitMQAddUserPointConfig;
import com.changgou.order.config.RabbitMQOrderAutoCloseConfig;
import com.changgou.order.dao.OrderConfigMapper;
import com.changgou.order.dao.OrderLogMapper;
import com.changgou.order.dao.OrderMapper;
import com.changgou.order.dao.TaskMapper;
import com.changgou.order.service.CartService;
import com.changgou.order.service.OrderItemService;
import com.changgou.order.service.OrderService;
import com.changgou.pay.feign.WeChatPayFeign;
import com.changgou.user.feign.UserFeign;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * OrderServiceImpl
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
@Slf4j
@Service
public class OrderServiceImpl implements OrderService {
    @Resource
    private IdWorker idWorker;

    @Resource
    private SkuFeign skuFeign;

    @Resource
    private UserFeign userFeign;

    @Resource
    private CartService cartService;

    @Resource
    private TaskMapper taskMapper;

    @Resource
    private OrderMapper orderMapper;

    @Resource
    private OrderLogMapper orderLogMapper;

    @Resource
    private OrderConfigMapper orderConfigMapper;

    @Resource
    private WeChatPayFeign weChatPayFeign;

    @Resource
    private RabbitTemplate rabbitTemplate;

    @Resource
    private OrderItemService orderItemService;

    @GlobalTransactional
    @Override
    public Order create(Order order) {
        log.info("Order Seata全局事务id=================>{}", RootContext.getXID());
        /*
         * TODO
         * 1.物品最新价格校验（异价时以数据库中最新价格为准）
         * 2.当前是和购物车绑定了，把购物车中的所有物品添加订单了，实际可以选择部分 done
         */
        List<OrderItem> items = cartService.list(order.getUsername());
        List<Long> skuList = order.getSkuList();
        // 只添加购物车选中的物品
        if (!CollectionUtils.isEmpty(skuList)) {
            items = cartService.filter(order.getUsername(), skuList);
            // 删除选中的购物车物品
            cartService.remove(order.getUsername(), skuList);
        }
        // 获取订单明细
        order.setUsername(order.getUsername());
        order.setId(idWorker.nextId() + "");
        int totalNum = 0;
        int totalMoney = 0;
        for (OrderItem item : items) {
            totalNum += item.getNum();
            totalMoney += item.getMoney();
            item.setId(idWorker.nextId() + "");
            item.setOrderId(order.getId());
            item.setIsReturn("0");
        }

        order.setTotalNum(totalNum);
        order.setTotalMoney(totalMoney);
        order.setPreMoney(totalMoney);
        order.setPayMoney(totalMoney);
        order.setPayType("1");
        order.setCreateTime(new Date());
        order.setUpdateTime(new Date());
        order.setPayTime(null);
        order.setSourceType("1");
        order.setTransactionId("?");
        order.setOrderStatus("0");
        order.setPayStatus("0");
        order.setIsDelete("0");

        order.setSkuList(null);

        orderMapper.insertSelective(order);

        for (OrderItem item : items) {
            orderItemService.add(item);
        }

        // 减库存
        Map<Long, Integer> map = items.stream().collect(Collectors.toMap(OrderItem::getSkuId, OrderItem::getNum));
        Result<String> decrease = skuFeign.decrease(map);
        if (!decrease.getCode().equals(StatusCode.OK)) {
            throw new ChanggouException(decrease.getCode(), decrease.getMessage());
        }

        // 添加积分
        Result<String> addPoints = userFeign.addPoints(totalMoney, order.getUsername());
        if (!addPoints.getCode().equals(StatusCode.OK)) {
            throw new ChanggouException(addPoints.getCode(), addPoints.getMessage());
        }
        // MQ方式分布式事务，只实现了添加用户积分的分布式事务
        // addTask(order);

        // 发送自动过期自动关单消息，TODO 异步发送
        rabbitTemplate.convertAndSend(RabbitMQOrderAutoCloseConfig.ORDER_AUTO_EXPIRE_EXCHANGE, "XA", order.getId());

        return order;
    }

    private void addTask(Order order) {
        // 增加任务表记录
        Task task = new Task();
        task.setCreateTime(new Date());
        task.setUpdateTime(new Date());
        task.setMqExchange(RabbitMQAddUserPointConfig.EX_BUYING_ADD_POINT_USER);
        task.setMqRoutingkey(RabbitMQAddUserPointConfig.KEY_BUYING_ADD_POINT_KEY);

        Map<String, Object> map = new HashMap<>();
        map.put("userName", order.getUsername());
        map.put("orderId", order.getId());
        map.put("point", order.getPayMoney());
        task.setRequestBody(JSON.toJSONString(map));
        taskMapper.insertSelective(task);
    }

    @Override
    public void take(String orderId, String operator) {
        Order order = orderMapper.selectByPrimaryKey(orderId);
        if (order == null) {
            throw new ChanggouException(ChanggouErrorCode.ORDER_NOT_EXIST);
        }
        if (!"1".equals(order.getConsignStatus())) {
            throw new ChanggouException(ChanggouErrorCode.ORDER_UN_SEND);
        }
        order.setConsignStatus("2"); // 已送达
        order.setOrderStatus("3");// 已完成
        order.setUpdateTime(new Date());
        order.setEndTime(new Date());// 交易结束
        orderMapper.updateByPrimaryKeySelective(order);
        // 记录订单变动日志
        OrderLog orderLog = new OrderLog();
        orderLog.setId(idWorker.nextId() + "");
        orderLog.setOperateTime(new Date());// 当前日期
        orderLog.setOperater(operator);// 系统？管理员？用户？
        orderLog.setOrderStatus("3");
        orderLog.setOrderId(order.getId());
        orderLogMapper.insertSelective(orderLog);
    }

    @Override
    @Transactional
    public void batchSend(List<Order> orders) {
        // TODO 对接第三方物流
        // 判断运单号和物流公司是否为空
        for (Order order : orders) {
            if (order.getId() == null) {
                throw new ChanggouException(ChanggouErrorCode.ORDER_NO_EMPTY);
            }
            if (order.getShippingCode() == null || order.getShippingName() == null) {
                throw new ChanggouException(ChanggouErrorCode.CHOSE_DELIVERY);
            }
        }

        // 循环订单,进行状态校验
        for (Order order : orders) {
            Order order1 = orderMapper.selectByPrimaryKey(order.getId());
            if (!"0".equals(order1.getConsignStatus()) || !"1".equals(order1.getOrderStatus())) {
                throw new ChanggouException(ChanggouErrorCode.ORDER_NO_WRONG);
            }
        }

        // 循环订单更新操作
        for (Order order : orders) {
            order.setOrderStatus("2");// 订单状态  已发货
            order.setConsignStatus("1"); // 发货状态  已发货
            order.setConsignTime(new Date());// 发货时间
            order.setUpdateTime(new Date());// 更新时间
            orderMapper.updateByPrimaryKeySelective(order);
            // 记录订单变动日志
            OrderLog orderLog = new OrderLog();
            orderLog.setId(idWorker.nextId() + "");
            orderLog.setOperateTime(new Date());// 当前日期
            orderLog.setOperater("admin");// 系统管理员
            orderLog.setOrderStatus("2"); // 已完成
            orderLog.setConsignStatus("1");// 发状态（0未发货 1已发货）
            orderLog.setOrderId(order.getId());
            orderLogMapper.insertSelective(orderLog);
        }
    }

    /**
     * TODO
     * 1.千万级数据量如何处理？
     * 2.如何控制更细力度的时间？
     */
    @Override
    @Transactional
    public void autoTack() {
        // 读取订单配置信息
        OrderConfig orderConfig = orderConfigMapper.selectByPrimaryKey(1);

        // 获得时间节点
        LocalDate now = LocalDate.now();// 当前日期

        // 获取过期的时间节点，在这个日期前发货的未收货订单都
        LocalDate date = now.plusDays(-orderConfig.getTakeTimeout());
        System.out.println(date);

        // 按条件查询过期订单
        Example example = new Example(Order.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andLessThan("consignTime", date);
        criteria.andEqualTo("orderStatus", "2");

        List<Order> orders = orderMapper.selectByExample(example);
        for (Order order : orders) {
            System.out.println("过期订单：" + order.getId() + " " + order.getConsignStatus());
            take(order.getId(), "system");
        }
    }

    /**
     * 增加Order
     *
     * @param order
     */
    @Override
    public void add(Order order) {
        orderMapper.insert(order);
    }

    /**
     * 删除
     *
     * @param id
     */
    @Override
    public void delete(String id) {
        orderMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void delete(String out_trade_no, String transaction_id, Date time_end) {
        // 1.根据id 获取订单的数据
        Order order = orderMapper.selectByPrimaryKey(out_trade_no);
        if (order == null) {
            log.warn("支付通知删除订单时查询不到订单 orderId={}", out_trade_no);
            return;
        }
        // 2.更新
        order.setUpdateTime(new Date());
        //  支付的时间 从微信的参数中获取
        order.setPayTime(time_end);
        order.setOrderStatus("1");
        order.setPayStatus("2");
        order.setTransactionId(transaction_id);

        // 3.更新到数据库
        orderMapper.updateByPrimaryKeySelective(order);

        // 4.修改库存
        OrderItem orderItem = new OrderItem();
        orderItem.setOrderId(out_trade_no);
        List<OrderItem> list = orderItemService.findList(orderItem);
        Map<Long, Integer> map = list.stream().collect(Collectors.toMap(OrderItem::getSkuId, item -> 1));
        skuFeign.increase(map);

        // TODO 5.关闭微信的支付订单，根据关单结果处理后续操作：如果关单失败（用户已经正常支付了）修改订单为已支付（微信关单结果错误码为ORDERPAID）；如果关单成功，修改订单为支付失败
        Result<Map<String, String>> result = weChatPayFeign.cls(out_trade_no);
    }

    /**
     * 修改Order
     *
     * @param order
     */
    @Override
    public void update(Order order) {
        orderMapper.updateByPrimaryKey(order);
    }

    @Override
    public void update(String out_trade_no, String transaction_id, Date time_end) {
        // 1.根据id获取订单的数据
        Order order = orderMapper.selectByPrimaryKey(out_trade_no);
        if (order == null) {
            log.warn("支付通知更新订单状态时查询不到订单 orderId={}", out_trade_no);
            return;
        }

        // 2.更新
        order.setUpdateTime(new Date());
        //  支付的时间 从微信的参数中获取
        order.setPayTime(time_end);
        order.setOrderStatus("1");
        order.setPayStatus("1");
        order.setTransactionId(transaction_id);

        // 3.更新到数据库
        orderMapper.updateByPrimaryKeySelective(order);
    }

    @Override
    public void close(String orderId) {
        // 1.根据id 获取订单的数据
        Order order = orderMapper.selectByPrimaryKey(orderId);
        if (order == null) {
            log.warn("关单时发现订单不存在 orderId={}", orderId);
            return;
        }

        /*
         * 2.查询微信支付的结果，支付时间从微信支付结果中获取
         * 1.如果支付成功，什么都不做
         * 2.如果支付失败，需要关闭微信支付订单，然后关单，回滚库存
         * 实际场景中还有很多操作
         */
        order.setUpdateTime(new Date());
        order.setPayTime(null);
        order.setOrderStatus("1");
        order.setPayStatus("2");
        order.setTransactionId("xxx");

        // 3.更新到数据库
        orderMapper.updateByPrimaryKeySelective(order);

        // 4.修改库存
        OrderItem orderItem = new OrderItem();
        orderItem.setOrderId(orderId);
        List<OrderItem> list = orderItemService.findList(orderItem);
        Map<Long, Integer> map = list.stream().collect(Collectors.toMap(OrderItem::getSkuId, item -> 1));
        skuFeign.increase(map);

        // TODO 5.关闭微信的支付订单，根据关单结果处理后续操作：如果关单失败（用户已经正常支付了）修改订单为已支付（微信关单结果错误码为ORDERPAID）；如果关单成功，修改订单为支付失败
        Result<Map<String, String>> result = weChatPayFeign.cls(orderId);
    }

    /**
     * 根据ID查询Order
     *
     * @param id
     * @return
     */
    @Override
    public Order findById(String id) {
        return orderMapper.selectByPrimaryKey(id);
    }

    /**
     * 查询Order全部数据
     *
     * @return
     */
    @Override
    public List<Order> findAll() {
        return orderMapper.selectAll();
    }

    /**
     * Order条件查询
     *
     * @param order
     * @return
     */
    @Override
    public List<Order> findList(Order order) {
        // 构建查询条件
        Example example = createExample(order);
        // 根据构建的条件查询数据
        return orderMapper.selectByExample(example);
    }

    /**
     * Order分页查询
     *
     * @param page
     * @param size
     * @return
     */
    @Override
    public PageInfo<Order> findPage(int page, int size) {
        // 静态分页
        PageHelper.startPage(page, size);
        // 分页查询
        return new PageInfo<Order>(orderMapper.selectAll());
    }

    /**
     * Order条件+分页查询
     *
     * @param order 查询条件
     * @param page  页码
     * @param size  页大小
     * @return 分页结果
     */
    @Override
    public PageInfo<Order> findPage(Order order, int page, int size) {
        // 分页
        PageHelper.startPage(page, size);
        // 搜索条件构建
        Example example = createExample(order);
        // 执行搜索
        return new PageInfo<Order>(orderMapper.selectByExample(example));
    }

    /**
     * Order构建查询对象
     *
     * @param order
     * @return
     */
    public Example createExample(Order order) {
        Example example = new Example(Order.class);
        Example.Criteria criteria = example.createCriteria();
        if (order != null) {
            // 订单id
            if (!StringUtils.isEmpty(order.getId())) {
                criteria.andEqualTo("id", order.getId());
            }
            // 数量合计
            if (!StringUtils.isEmpty(order.getTotalNum())) {
                criteria.andEqualTo("totalNum", order.getTotalNum());
            }
            // 金额合计
            if (!StringUtils.isEmpty(order.getTotalMoney())) {
                criteria.andEqualTo("totalMoney", order.getTotalMoney());
            }
            // 优惠金额
            if (!StringUtils.isEmpty(order.getPreMoney())) {
                criteria.andEqualTo("preMoney", order.getPreMoney());
            }
            // 邮费
            if (!StringUtils.isEmpty(order.getPostFee())) {
                criteria.andEqualTo("postFee", order.getPostFee());
            }
            // 实付金额
            if (!StringUtils.isEmpty(order.getPayMoney())) {
                criteria.andEqualTo("payMoney", order.getPayMoney());
            }
            // 支付类型，1、在线支付、0 货到付款
            if (!StringUtils.isEmpty(order.getPayType())) {
                criteria.andEqualTo("payType", order.getPayType());
            }
            // 订单创建时间
            if (!StringUtils.isEmpty(order.getCreateTime())) {
                criteria.andEqualTo("createTime", order.getCreateTime());
            }
            // 订单更新时间
            if (!StringUtils.isEmpty(order.getUpdateTime())) {
                criteria.andEqualTo("updateTime", order.getUpdateTime());
            }
            // 付款时间
            if (!StringUtils.isEmpty(order.getPayTime())) {
                criteria.andEqualTo("payTime", order.getPayTime());
            }
            // 发货时间
            if (!StringUtils.isEmpty(order.getConsignTime())) {
                criteria.andEqualTo("consignTime", order.getConsignTime());
            }
            // 交易完成时间
            if (!StringUtils.isEmpty(order.getEndTime())) {
                criteria.andEqualTo("endTime", order.getEndTime());
            }
            // 交易关闭时间
            if (!StringUtils.isEmpty(order.getCloseTime())) {
                criteria.andEqualTo("closeTime", order.getCloseTime());
            }
            // 物流名称
            if (!StringUtils.isEmpty(order.getShippingName())) {
                criteria.andEqualTo("shippingName", order.getShippingName());
            }
            // 物流单号
            if (!StringUtils.isEmpty(order.getShippingCode())) {
                criteria.andEqualTo("shippingCode", order.getShippingCode());
            }
            // 用户名称
            if (!StringUtils.isEmpty(order.getUsername())) {
                criteria.andLike("username", "%" + order.getUsername() + "%");
            }
            // 买家留言
            if (!StringUtils.isEmpty(order.getBuyerMessage())) {
                criteria.andEqualTo("buyerMessage", order.getBuyerMessage());
            }
            // 是否评价
            if (!StringUtils.isEmpty(order.getBuyerRate())) {
                criteria.andEqualTo("buyerRate", order.getBuyerRate());
            }
            // 收货人
            if (!StringUtils.isEmpty(order.getReceiverContact())) {
                criteria.andEqualTo("receiverContact", order.getReceiverContact());
            }
            // 收货人手机
            if (!StringUtils.isEmpty(order.getReceiverMobile())) {
                criteria.andEqualTo("receiverMobile", order.getReceiverMobile());
            }
            // 收货人地址
            if (!StringUtils.isEmpty(order.getReceiverAddress())) {
                criteria.andEqualTo("receiverAddress", order.getReceiverAddress());
            }
            // 订单来源：1:web，2：app，3：微信公众号，4：微信小程序  5 H5手机页面
            if (!StringUtils.isEmpty(order.getSourceType())) {
                criteria.andEqualTo("sourceType", order.getSourceType());
            }
            // 交易流水号
            if (!StringUtils.isEmpty(order.getTransactionId())) {
                criteria.andEqualTo("transactionId", order.getTransactionId());
            }
            // 订单状态,0:未完成,1:已完成，2：已退货
            if (!StringUtils.isEmpty(order.getOrderStatus())) {
                criteria.andEqualTo("orderStatus", order.getOrderStatus());
            }
            // 支付状态,0:未支付，1：已支付，2：支付失败
            if (!StringUtils.isEmpty(order.getPayStatus())) {
                criteria.andEqualTo("payStatus", order.getPayStatus());
            }
            // 发货状态,0:未发货，1：已发货，2：已收货
            if (!StringUtils.isEmpty(order.getConsignStatus())) {
                criteria.andEqualTo("consignStatus", order.getConsignStatus());
            }
            // 是否删除
            if (!StringUtils.isEmpty(order.getIsDelete())) {
                criteria.andEqualTo("isDelete", order.getIsDelete());
            }
        }
        return example;
    }

}
