package com.changgou.order.dao;

import com.changgou.order.bean.UndoLog;
import tk.mybatis.mapper.common.Mapper;

/**
 * UndoLogDao
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
public interface UndoLogMapper extends Mapper<UndoLog> {

}
