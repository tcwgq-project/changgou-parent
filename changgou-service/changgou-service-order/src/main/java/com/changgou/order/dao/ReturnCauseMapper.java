package com.changgou.order.dao;

import com.changgou.order.bean.ReturnCause;
import tk.mybatis.mapper.common.Mapper;

/**
 * ReturnCauseDao
 *
 * @author tcwgq
 * @since 2022/06/14 15:20
 */
public interface ReturnCauseMapper extends Mapper<ReturnCause> {

}
