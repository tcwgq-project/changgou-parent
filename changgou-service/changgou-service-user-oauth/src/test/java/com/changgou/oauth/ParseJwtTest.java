package com.changgou.oauth;

import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaVerifier;
import org.springframework.security.rsa.crypto.KeyStoreKeyFactory;

import java.security.KeyPair;
import java.security.interfaces.RSAPublicKey;

/**
 * @author tcwgq
 * @since 2022/6/13 20:46
 */
public class ParseJwtTest {
    @Test
    public void parse() {
        String token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJhcHAiXSwibmFtZSI6bnVsbCwiaWQiOm51bGwsImV4cCI6MjA4NzE5OTQ1NiwiYXV0aG9yaXRpZXMiOlsiYWRtaW4iLCJ1c2VyIl0sImp0aSI6IjIwOWRmMGUyLTcxMGYtNGM3My04OTYwLTM4NjdlZjNmNDMyYyIsImNsaWVudF9pZCI6ImNoYW5nZ291IiwidXNlcm5hbWUiOiJzeml0aGVpbWEifQ.SNz_0qYT2FFL4Sq23UVGZB72L2_h6FQerB9yjRqR5WfT6etWUvgSPwcWaKEMW1H4PdRJsMG-EQxS8SHyFSBY3AVAR2GKcyevdAnI_FxvYluQhLU381npLw194yf5wSEYMLUTfYULyDPHP4bE-pcHGcevi3HCCLkMaos4phYhxWy1JbKp1OKyWZQenSq-th6FqO2LiHeaXVJUkoK2PIiBriD7ZEcSQr_8K8WT-VK_g-0dG5Cug2SC3raL9fdJTvoXr_kkqejCpCKy0xNrqC529RSlRUYJ_SFTwd_toon4lwjz4vM5VNa5DhrCh2Y8ZzaeHxziQp_WTHXtSwlk6gs37g";
        ClassPathResource resource = new ClassPathResource("changgou.jks");
        // 读取证书文件
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(resource, "changgou".toCharArray());
        KeyPair keyPair = keyStoreKeyFactory.getKeyPair("changgou", "changgou".toCharArray());
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        Jwt jwt = JwtHelper.decodeAndVerify(token, new RsaVerifier(publicKey));
        System.out.println(jwt);
        System.out.println(jwt.getEncoded());
    }

}
