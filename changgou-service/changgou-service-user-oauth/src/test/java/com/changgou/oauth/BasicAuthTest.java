package com.changgou.oauth;

import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * @author tcwgq
 * @since 2022/6/13 21:44
 */
public class BasicAuthTest {
    @Test
    public void test1() {
        String basic = "Y2hhbmdnb3U6Y2hhbmdnb3U=";
        byte[] decode = Base64.getDecoder().decode(basic.getBytes(StandardCharsets.UTF_8));
        System.out.println(new String(decode));
    }

}
