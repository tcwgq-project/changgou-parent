package com.changgou.oauth.service.impl;

import com.changgou.oauth.bean.UserJwt;
import com.changgou.user.feign.UserFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * 自定义授权认证类
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserFeign userFeign;

    @Autowired
    ClientDetailsService clientDetailsService;

    public static void main(String[] args) {
        String zhangSan = new BCryptPasswordEncoder().encode("zhangSan");
        System.out.println(zhangSan);
    }

    /**
     * 自定义授权认证
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 客户端认证，取出身份，如果身份为空说明没有认证
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // 没有认证统一采用httpBasic认证，httpBasic中存储了client_id和client_secret，开始认证client_id和client_secret
        if (authentication == null) {
            ClientDetails clientDetails = clientDetailsService.loadClientByClientId(username);
            if (clientDetails != null) {
                // 秘钥
                String clientSecret = clientDetails.getClientSecret();
                // 静态方式
                // return new User(username, new BCryptPasswordEncoder().encode(clientSecret), AuthorityUtils.commaSeparatedStringToAuthorityList(""));
                // 数据库查找方式
                return new User(username, clientSecret, AuthorityUtils.commaSeparatedStringToAuthorityList(""));
            }
        }

        // 用户信息认证
        if (StringUtils.isEmpty(username)) {
            return null;
        }

        // 根据用户名查询用户信息，注意有可能为空
        // String pwd = new BCryptPasswordEncoder().encode("szitheima");
        String pwd = userFeign.loadById(username).getData().getPassword();
        // 创建User对象  授予权限.GOODS_LIST  SECKILL_LIST
        String permissions = "user,admin";
        return new UserJwt(username, pwd, AuthorityUtils.commaSeparatedStringToAuthorityList(permissions));
    }

}
