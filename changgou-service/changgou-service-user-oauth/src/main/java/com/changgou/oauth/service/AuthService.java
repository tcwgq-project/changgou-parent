package com.changgou.oauth.service;

import com.changgou.oauth.bean.AuthToken;

/**
 * @since 2019/7/7 16:23
 */
public interface AuthService {

    /**
     * 授权认证方法
     */
    AuthToken login(String username, String password, String clientId, String clientSecret);

}
