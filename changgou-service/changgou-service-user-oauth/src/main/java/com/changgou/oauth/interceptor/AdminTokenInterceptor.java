package com.changgou.oauth.interceptor;

import com.changgou.oauth.util.AdminToken;
import feign.RequestInterceptor;
import feign.RequestTemplate;

/**
 * @author tcwgq
 * @since 2022/6/14 17:47
 */
// @Component
public class AdminTokenInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate template) {
        /*
         * TODO 是否可以考虑使用原始的token？用户还未登陆，不存在原始token；如果登陆过了，有原始token
         * 不推荐为了调一个用户查询接口专门使用一个token，不如在user-service那边配置个特殊的url，不进行权限拦截，专门给登陆使用
         */
        String token = AdminToken.create();
        template.header("Authorization", "bearer " + token);
    }

}
