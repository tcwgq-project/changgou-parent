package com.changgou.oauth.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@Order(-1)
class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * 忽略安全拦截的URL
     */
    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers(
                "/oauth/toLogin", "/oauth/login", "/oauth/logout", "/css/**", "/data/**", "/fonts/**", "/img/**", "/js/**");
    }

    /**
     * 创建授权管理认证对象
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        super.configure(auth);
    }

    /**
     * 采用BCryptPasswordEncoder对密码进行编码
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .httpBasic()        // 启用Http基本身份验证
                .and()
                .formLogin()       // 启用表单身份验证
                // 自定义登陆页面
                .loginPage("/oauth/toLogin") // 配置Spring Security要跳转登陆页面的访问路径
                .loginProcessingUrl("/oauth/login") // 配置Spring Security执行登陆操作的访问路径
                .and()
                .authorizeRequests()    // 限制基于Request请求访问
                .anyRequest()
                .authenticated();       // 其他请求都需要经过验证
    }

}
