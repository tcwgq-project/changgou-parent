package com.changgou.oauth.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 描述
 *
 * @version 1.0
 * @since 1.0
 */
@Controller
@RequestMapping("/oauth")
public class LoginRedirectController {
    @GetMapping("/toLogin")
    public String login(String from, Model model) {
        model.addAttribute("from", from);
        return "login";
    }

}
