package com.changgou.file.utils;

import org.csource.fastdfs.FileInfo;
import org.csource.fastdfs.ServerInfo;
import org.csource.fastdfs.StorageServer;
import org.junit.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

/**
 * @author tcwgq
 * @since 2022/5/27 15:52
 */
public class FastDFSClientTest {

    @Test
    public void upload() {
    }

    @Test
    public void getFile() {
        FileInfo fileInfo = FastDFSClient.getFile("group1", "M00/00/00/wKj1g2KQeAWAS_xjAAWjAlet-v8930.jpg");
        System.out.println(fileInfo);
    }

    @Test
    public void downFile() throws IOException {
        InputStream inputStream = FastDFSClient.downFile("group1", "M00/00/00/wKj1g2KQeAWAS_xjAAWjAlet-v8930.jpg");
        int index;
        byte[] bytes = new byte[1024];
        FileOutputStream outputStream = new FileOutputStream("F:/abc.jpg");
        while ((index = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, index);
            outputStream.flush();
        }
        outputStream.close();
        inputStream.close();
    }

    @Test
    public void deleteFile() throws Exception {
        FastDFSClient.deleteFile("group1", "M00/00/00/wKj1g2KQiDGAfFlIAAed-MP8ZNA930.jpg");
    }

    @Test
    public void getStoreStorage() throws IOException {
        StorageServer storageServer = FastDFSClient.getStoreStorage("group1");
        System.out.println(storageServer);
    }

    @Test
    public void getStoreStorages() throws IOException {
        StorageServer[] storages = FastDFSClient.getStoreStorages("group1");
        Arrays.stream(storages).forEach(storageServer -> System.out.println(storageServer.getInetSocketAddress().getHostString()));
    }

    @Test
    public void getFetchStorages() throws IOException {
        ServerInfo[] serverInfos = FastDFSClient.getFetchStorages("group1", "M00/00/00/wKj1g2KQjLyAD-ewAAWslyCZHMI272.jpg");
        Arrays.stream(serverInfos).forEach(serverInfo -> System.out.println(serverInfo.getIpAddr() + ":" + serverInfo.getPort()));
    }

    @Test
    public void getTrackerUrl() {
    }

}