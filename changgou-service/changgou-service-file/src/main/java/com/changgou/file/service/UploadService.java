package com.changgou.file.service;

import com.changgou.file.bean.FastDFSFile;

import java.io.IOException;

/**
 * @author tcwgq
 * @since 2022/5/27 14:42
 */
public interface UploadService {
    String upload(FastDFSFile file) throws IOException;

}
