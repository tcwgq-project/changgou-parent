package com.changgou.file.service.impl;

import com.changgou.file.bean.FastDFSFile;
import com.changgou.file.service.UploadService;
import com.changgou.file.utils.FastDFSClient;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * @author tcwgq
 * @since 2022/5/27 14:43
 */
@Service
public class UploadServiceImpl implements UploadService {
    @Override
    public String upload(FastDFSFile file) throws IOException {
        String[] strings = FastDFSClient.upload(file);
        // 封装返回结果
        return FastDFSClient.getTrackerUrl() + strings[0] + "/" + strings[1];
    }

}
